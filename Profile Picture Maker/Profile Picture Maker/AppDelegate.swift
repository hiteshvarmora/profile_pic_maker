//
//  AppDelegate.swift
//  Profile Picture Maker
//
//  Created by Apple on 15/06/21.
//

import UIKit
import Firebase
import Purchases //RevenueCat

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

     var window: UIWindow? {
        guard let scene = UIApplication.shared.connectedScenes.first,
              let windowSceneDelegate = scene.delegate as? UIWindowSceneDelegate,
              let window = windowSceneDelegate.window else {
            return nil
        }
        return window
    }
    
    var appLanguageLocalizeKeyName:String!

    var isRevenuecat = true //RevenueCat
    var offering : Purchases.Offering? //RevenueCat

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let langageRegion = Locale.preferredLanguages.first!
        let languageDic = Locale.components(fromIdentifier: langageRegion)
        appLanguageLocalizeKeyName = languageDic["kCFLocaleLanguageCodeKey"]
        
        Purchases.debugLogsEnabled = true
        Purchases.automaticAppleSearchAdsAttributionCollection = true
        Purchases.configure(withAPIKey: "pomNGwxFYwnQtMGLKgkurCrifxRFTCqt")

        getPriceforSales()
        
        FirebaseApp.configure()
                
        return true
    }
    
    func setRootViewController() {
        let obj = StoryBoardName.home.instantiateViewController(identifier: "navigation") as! UINavigationController
        guard let scene = UIApplication.shared.connectedScenes.first,
              let windowSceneDelegate = scene.delegate as? UIWindowSceneDelegate,
              let window = windowSceneDelegate.window else {
            return
        }
        window?.rootViewController = obj
        window?.makeKeyAndVisible()
    }

    func getPriceforSales() {
        
        //UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        InAppShared.shared.getPriceInfo(isRevenueCat: self.isRevenuecat, completion: { products in
            //UIApplication.shared.isNetworkActivityIndicatorVisible = false
            print(products)
            AppUtil.arrPriceIAPLocale = products.map({ (product) -> InAppPriceModel in
                return InAppPriceModel.init(product: product)
            })

            if(UserDefaults.standard.isWeekly == true)
            {
                InAppShared.shared.currentID = WEEKLY_SUBSCRIPTION_PRODUCT_ID
                self.configureStore(WEEKLY_SUBSCRIPTION_PRODUCT_ID)
            }
            
            if(UserDefaults.standard.isYearly == true)
            {
                InAppShared.shared.currentID = YEARLY_SUBSCRIPTION_PRODUCT_ID
                self.configureStore(YEARLY_SUBSCRIPTION_PRODUCT_ID)
            }
            
//            if !self.isLaunchFromNotification{
//                if !UserDefaults.standard.isSubscribed && UserDefaults.standard.isOnboardingShown {
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
//                        self.fetchConfig {
//                            let anObj = StoryBoardName.main.instantiateViewController(withIdentifier: UserDefaults.standard.salesPage == 0 ? "NewSalesVC" : "AnimatedSalesVC")
//                            if (self.window!.rootViewController?.presentedViewController as? DiscountedSalesVC) != nil {
//                                return
//                            }
//                            let aNavController = UINavigationController(rootViewController: anObj)
//                            aNavController.modalPresentationStyle = .fullScreen
//                            aNavController.navigationBar.isHidden = true
//                            self.window!.rootViewController?.present(aNavController, animated: true)
//                        }
//                    }
//                }
//            }else{
//                self.isLaunchFromNotification = false
//            }
            
            NotificationCenter.default.post(name: NSNotification.Name("inAppPricesUpdated"), object: nil)
            
        })
    }

    func configureStoreForRestore(isRevenueCat:Bool,completion:@escaping ()->())
    {
            Purchases.shared.purchaserInfo { (purchaserInfo, error) in
                if purchaserInfo?.entitlements.all["UNLIMITED ACCESS"]?.isActive == true {
                    UserDefaults.standard.isWeekly = false
                    UserDefaults.standard.isYearly =  false
                    // Grant user "pro" access
                    let strProduct : String = purchaserInfo?.entitlements.all["UNLIMITED ACCESS"]?.productIdentifier ?? ""
                    
                    switch strProduct
                    {
                    case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                        UserDefaults.standard.isWeekly = true
                    case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                        UserDefaults.standard.isYearly =  true
                    default: break
                    }
                    
                }
                if purchaserInfo?.entitlements.all["UNLIMITED ACCESS"]?.isActive == true
                {
                    if(UserDefaults.standard.isWeekly || UserDefaults.standard.isYearly)
                    {
                        if(UserDefaults.standard.isWeekly)
                        {
                            InAppShared.shared.currentID = WEEKLY_SUBSCRIPTION_PRODUCT_ID
                        }
                        else if(UserDefaults.standard.isYearly)
                        {
                            InAppShared.shared.currentID = YEARLY_SUBSCRIPTION_PRODUCT_ID
                        }
                        else
                        {
                            InAppShared.shared.currentID = ""
                        }
                        UserDefaults.standard.isSubscribed = true
                        UserDefaults.standard.subscriptionDate = Date()
                    }
                    else
                    {
                        InAppShared.shared.currentID = ""
                        UserDefaults.standard.isSubscribed = false
                        UserDefaults.standard.subscriptionDate = nil
                    }
                }
                else{
                    InAppShared.shared.currentID = ""
                    UserDefaults.standard.isSubscribed = false
                    UserDefaults.standard.subscriptionDate = nil
                }
                completion()
            }
    }
         
    func configureStore(_ productIdentifier: String) {
       
        InAppShared.shared.verifyReceipt(isRevenueCat: Constants.appDelegate.isRevenuecat, completion: { isSuccess, expiryDate in
            
            switch productIdentifier
            {
            case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                UserDefaults.standard.isWeekly = isSuccess ? true : false
            case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                UserDefaults.standard.isYearly = isSuccess ? true : false
            default: break
                
            }
            UserDefaults.standard.isSubscribed = isSuccess ? true : false
            UserDefaults.standard.subscriptionDate = isSuccess ? Date() : nil
            
        }, errorBlock: { msg, status in
            if status == 0 {
                switch productIdentifier
                {
                case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                    UserDefaults.standard.isWeekly = false
                case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                    UserDefaults.standard.isYearly = false
                default: break
                    
                }
                UserDefaults.standard.isSubscribed = false
                UserDefaults.standard.subscriptionDate = nil
            } else {
                print("\(msg)")
            }
        })
    }


    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    class func sharedInstance() -> (AppDelegate){
      return UIApplication.shared.delegate as! AppDelegate
    }

}

