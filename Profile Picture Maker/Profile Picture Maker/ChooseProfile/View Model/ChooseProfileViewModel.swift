//  
//  ChooseProfileViewModel.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 19/06/21.
//

import Foundation
import UIKit
import Photos

class ChooseProfileViewModel {

    let model = DeepLabV3()
    let context = CIContext(options: nil)

    private let service: ChooseProfileServiceProtocol

    var arrChooseProfile: [ChooseProfileModel] = [ChooseProfileModel]()

    var images = [UIImage]()
    
    var allPhotos : PHFetchResult<PHAsset>? = nil
    var selectedIndex : IndexPath?
    
    //MARK: -- UI Status
    
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.checkPhotosPermission()
        }
    }

    /// Define selected model
    var selectedObject: ChooseProfileModel?

    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?

    init(withChooseProfile serviceProtocol: ChooseProfileServiceProtocol = ChooseProfileService() ) {
        self.service = serviceProtocol
    }
    
    fileprivate func checkPhotosPermission() {
        PHPhotoLibrary.requestAuthorization { (status) in
            switch status {
            case .authorized:
                print("You Are Authrized To Access")
                let fetchOptions = PHFetchOptions()
                let sortOrder = [NSSortDescriptor(key: "creationDate", ascending: false)]
                fetchOptions.sortDescriptors = sortOrder

                self.allPhotos = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                
                self.didGetData?()
                //self.getPhotosFromAlbum()
            case .denied, .restricted:
                print("Not allowed")
            case .notDetermined:
                print("Not determined yet")
            case .limited:
                print("You Are Authrized To Access")
                let fetchOptions = PHFetchOptions()
                let sortOrder = [NSSortDescriptor(key: "creationDate", ascending: false)]
                fetchOptions.sortDescriptors = sortOrder

                self.allPhotos = PHAsset.fetchAssets(with: .image, options: nil)
                self.didGetData?()
                //self.getPhotosFromAlbum()
            @unknown default:
                print("Default")
            }
        }
    }
    
    func getPhotosFromAlbum() {
        let manager = PHImageManager.default()
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        let results: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if results.count > 0 {
            for i in 0..<results.count {
                let asset = results.object(at: i)
                let size = CGSize(width: 700, height: 700)
                manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: requestOptions) { (image, _) in
                    if let image = image {
                        self.images.append(image)
                    } else {
                        print("error asset to image")
                    }
                }
            }
            self.didGetData?()
        } else {
            print("no photos to display")
        }
    }
}

extension UIImageView {
    func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize) {
        let options = PHImageRequestOptions()
        PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, info in
            
            if let info = info, info["PHImageFileUTIKey"] == nil {
                if let isDegraded = info[PHImageResultIsDegradedKey] as? Bool, isDegraded {
                    //Here is Low quality image , in this case return
                    print("PHImageResultIsDegradedKey =======> \(isDegraded)")
                    return
                }
                //Here you got high resilutions image
            }
            
            guard let image = image else { return }
            switch contentMode {
            case .aspectFill:
                self.contentMode = .scaleAspectFill
            case .aspectFit:
                self.contentMode = .scaleAspectFit
            @unknown default:
                print("default")
            }
            self.image = image
        }
        
    }
}

extension ChooseProfileViewModel {
    func removeBackground(image:UIImage) -> UIImage?{
        let resizedImage = image.resized(to: CGSize(width: 513, height: 513))
        if let pixelBuffer = resizedImage.pixelBuffer(width: Int(resizedImage.size.width), height: Int(resizedImage.size.height)){
            if let outputImage = (try? model.prediction(image: pixelBuffer))?.semanticPredictions.image(min: 0, max: 1, axes: (0,0,1)), let outputCIImage = CIImage(image:outputImage){
                if let maskImage = removeWhitePixels(image:outputCIImage), let resizedCIImage = CIImage(image: resizedImage), let compositedImage = composite(image: resizedCIImage, mask: maskImage){
                    return UIImage(ciImage: compositedImage).resized(to: CGSize(width: image.size.width, height: image.size.height))
                }
            }
        }
        return nil
    }
    
    private func removeWhitePixels(image:CIImage) -> CIImage?{
        let chromaCIFilter = chromaKeyFilter()
        chromaCIFilter?.setValue(image, forKey: kCIInputImageKey)
        return chromaCIFilter?.outputImage
    }
    
    private func composite(image:CIImage,mask:CIImage) -> CIImage?{
        return CIFilter(name:"CISourceOutCompositing",parameters:
            [kCIInputImageKey: image,kCIInputBackgroundImageKey: mask])?.outputImage
    }
    
    // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
    private func chromaKeyFilter() -> CIFilter? {
        let size = 64
        var cubeRGB = [Float]()
        
        for z in 0 ..< size {
            let blue = CGFloat(z) / CGFloat(size-1)
            for y in 0 ..< size {
                let green = CGFloat(y) / CGFloat(size-1)
                for x in 0 ..< size {
                    let red = CGFloat(x) / CGFloat(size-1)
                    let brightness = getBrightness(red: red, green: green, blue: blue)
                    let alpha: CGFloat = brightness == 1 ? 0 : 1
                    cubeRGB.append(Float(red * alpha))
                    cubeRGB.append(Float(green * alpha))
                    cubeRGB.append(Float(blue * alpha))
                    cubeRGB.append(Float(alpha))
                }
            }
        }
        
        let data = Data(buffer: UnsafeBufferPointer(start: &cubeRGB, count: cubeRGB.count))
        
        let colorCubeFilter = CIFilter(name: "CIColorCube", parameters: ["inputCubeDimension": size, "inputCubeData": data])
        return colorCubeFilter
    }
    
    // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
    private func getBrightness(red: CGFloat, green: CGFloat, blue: CGFloat) -> CGFloat {
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        var brightness: CGFloat = 0
        color.getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
        return brightness
    }

}
