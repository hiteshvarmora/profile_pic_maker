//
//  ChooseProfileCollectionViewCell.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 19/06/21.
//

import UIKit

class ChooseProfileCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgGalleryPhoto: UIImageView!
    
    @IBOutlet var constImageHeight: NSLayoutConstraint!
    @IBOutlet var constImageWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

}
