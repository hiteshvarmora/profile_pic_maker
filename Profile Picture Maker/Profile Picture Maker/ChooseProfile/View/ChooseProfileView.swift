//  
//  ChooseProfileView.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 19/06/21.
//

import UIKit
import Lottie
import Alamofire

class ChooseProfileView: UIViewController {

    // OUTLETS HERE
    @IBOutlet var collectionGallery: UICollectionView! {
        didSet{
            //collectionGallery.register(ChooseProfileCollectionViewCell.self, forCellWithReuseIdentifier: "idChooseProfileCollectionViewCell")
            collectionGallery.delegate = self
            collectionGallery.dataSource = self
        }
    }
    private var processingView: ProcessingView?

    // VARIABLES HERE
    var viewModel = ChooseProfileViewModel()
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configView()
        
        self.setupViewModel()
        
        NotificationCenter.default.addObserver(self, selector: #selector(openSalesPage), name: NSNotification.Name.init("inAppPricesUpdated"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Select Photo"
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.title = ""
    }
    
    @objc private func openSalesPage() {
        if !UserDefaults.standard.isSubscribed && UserDefaults.standard.isOnboardingShown {
            let salesVC : SalesPageView = StoryBoardName.sales.instantiateViewController(identifier: "SalesPageView") as! SalesPageView
            salesVC.modalPresentationStyle = .fullScreen
            
            self.present(salesVC, animated: true, completion: nil)
        }
        
    }

    
    fileprivate func setupViewModel() {
        self.viewModel.isLoading = true
        
        self.viewModel.updateLoadingStatus = {
            if self.viewModel.isLoading {
                print("LOADING...")
            } else {
                 print("DATA READY")
            }
        }

        self.viewModel.internetConnectionStatus = {
            print("Internet disconnected")
            // show UI Internet is disconnected
        }

        self.viewModel.serverErrorStatus = {
            print("Server Error / Unknown Error")
            // show UI Server is Error
        }

        self.viewModel.didGetData = {
            // update UI after get data
            DispatchQueue.main.async {
                self.collectionGallery.reloadData()
            }
        }

    }
    
    func configView() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.customAppearance()
        //collectionGallery.registerClass(ChooseProfileCollectionViewCell.self, forCellWithReuseIdentifier: "idChooseProfileCollectionViewCell")
    }
}

extension ChooseProfileView {
    
    private func showProcessingView() {
        
        processingView = ProcessingView.fromNib()
        
        let keyWindow = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first

        processingView?.frame = keyWindow!.bounds
        keyWindow!.addSubview(processingView!)
        
        processingView!.startAnimation()
    }
    
    private func hideProcessingView() {
        
        if let view = processingView {
            view.stopAnimation()
            view.removeFromSuperview()
            processingView = nil
        }
    }
}


extension ChooseProfileView:  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.allPhotos?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "idChooseProfileCollectionViewCell", for: indexPath) as! ChooseProfileCollectionViewCell
        
        if let index = self.viewModel.selectedIndex, index == indexPath {
            cell.contentView.borderWidth = 4.0
            cell.contentView.borderColor = UIColor(named: "clrViolet")
        } else {
            cell.contentView.borderWidth = 0.0
            cell.contentView.borderColor = .clear
        }
        
        let size = (ScreenSize.SCREEN_WIDTH / 3) - 15
        cell.constImageWidth.constant = size
        cell.constImageHeight.constant = size
        
        if cell.imgGalleryPhoto != nil {
            /*
            if let imgData = viewModel.images[indexPath.row].pngData() {
                cell.imgGalleryPhoto.image = UIImage(data: imgData)
            }*/
            
            let asset = viewModel.allPhotos?.object(at: indexPath.row)
            cell.imgGalleryPhoto.fetchImage(asset: asset!, contentMode: .aspectFill, targetSize: cell.imgGalleryPhoto.frame.size)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.selectedIndex = indexPath
        self.collectionGallery.reloadData()
        
        let asset = viewModel.allPhotos?.object(at: indexPath.row)
        
        self.showProcessingView()
        
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            fetchImage(asset: asset!, contentMode: .aspectFit, targetSize: CGSize(width: 512,height: 512)) { [self] (image) in
                if image != nil {
                    let img = image!.removeBackground(returnResult: .finalImage)//self.viewModel.removeBackground(image: image!)
                    
                    //callsendImageAPI(image: image!) { image in
                        if img != nil {
                            DispatchQueue.main.async {
                                self.hideProcessingView()
                                
                                //let exportProfileVC : ExportProfileView = StoryBoardName.exportProfile.instantiateViewController(withIdentifier: "ExportProfileView") as! ExportProfileView
                                //exportProfileVC.image = img!
                                //self.navigationController?.pushViewController(exportProfileVC, animated: true)
                                
                                let editVC : EditProfileView = StoryBoardName.editProfile.instantiateViewController(withIdentifier: "EditProfileView") as! EditProfileView
                                editVC.image = img!
                                self.navigationController?.pushViewController(editVC, animated: true)
                            }
                        }
                        else{
                            DispatchQueue.main.async {
                                self.hideProcessingView()
                            }
                        }
                    //}
                    //let img = self.viewModel.removeBackground(image: image!)
                    
                    
                }
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (ScreenSize.SCREEN_WIDTH / 3) - 15
        return CGSize(width: size, height: size)
    }
    
}

extension ChooseProfileView {
    
    
    func callsendImageAPI(image:UIImage, completionHandler : @escaping (UIImage?) -> ()) {
        let headers: HTTPHeaders
        headers = [
            "content-type": "multipart/form-data; boundary=---011000010111000001101001",
            "Content-Disposition" : "form-data",
            "x-rapidapi-host" : "image-background-removal-v2.p.rapidapi.com"
        ]
        AF.upload(multipartFormData: { (multipartFormData) in
        guard let imgData = image.jpegData(compressionQuality: 1) else { return }
        multipartFormData.append(imgData, withName: "image_file", fileName: "file.jpg", mimeType: "image/jpeg")
        },to: URL.init(string: "https://api.remove.bg/v1.0/removebg")!, usingThreshold: UInt64.init(),
            method: .post,
            headers: headers).response{ response in
                if (response.error == nil) {
                    if let jsonData = response.data{
                        let image = UIImage(data: jsonData)
                        
                        completionHandler(image)
//                        self.outputImage.image = image
                    }
                }else{
                    completionHandler(nil)
                    print(response.error?.localizedDescription ?? "")
                }
        }
    }

    func callsendImageAPI1(image:UIImage, completionHandler : @escaping (UIImage?) -> ()) {
        let headers: HTTPHeaders
        headers = [
            "Content-type": "multipart/form-data",
            "Content-Disposition" : "form-data",
            "X-Api-Key" : "bk589utcJbGZjrGvZknGyDRU"
        ]
        AF.upload(multipartFormData: { (multipartFormData) in
        guard let imgData = image.jpegData(compressionQuality: 1) else { return }
        multipartFormData.append(imgData, withName: "image_file", fileName: "file.jpg", mimeType: "image/jpeg")
        },to: URL.init(string: "https://api.remove.bg/v1.0/removebg")!, usingThreshold: UInt64.init(),
            method: .post,
            headers: headers).response{ response in
                if (response.error == nil) {
                    if let jsonData = response.data{
                        let image = UIImage(data: jsonData)
                        
                        completionHandler(image)
//                        self.outputImage.image = image
                    }
                }else{
                    completionHandler(nil)
                    print(response.error?.localizedDescription ?? "")
                }
        }
    }

}
