//
//  ProcessingView.swift
//  Profile Picture Maker
//
//  Created by Apple on 28/06/21.
//

import UIKit
import Lottie

class ProcessingView: UIView {
    
    @IBOutlet weak var boxView : UIView!
    private var animationView : AnimationView?
    
    func startAnimation() {
        let filepath = Bundle.main.path(forResource: "image", ofType: "json")
        
        animationView = AnimationView(filePath: filepath!)
        
        animationView?.frame = boxView.bounds
        // 3. Set animation content mode
        animationView!.contentMode = .scaleAspectFit
        // 4. Set animation loop mode
        animationView!.loopMode = .loop
        // 5. Adjust animation speed
        animationView!.animationSpeed = 0.5
        animationView!.backgroundColor = UIColor.clear
        
        boxView.addSubview(animationView!)
        // 6. Play animation
        animationView!.play()
    }
    
    func stopAnimation() {
        if let animationview = animationView {
            animationview.stop()
        }
    }
}
