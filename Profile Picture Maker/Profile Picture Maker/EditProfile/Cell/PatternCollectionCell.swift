//
//  PatternCollectionCell.swift
//  Profile Picture Maker
//
//  Created by Apple on 26/06/21.
//

import UIKit

class PatternCollectionCell: UICollectionViewCell {

    @IBOutlet var bgImage: UIImageView!
    @IBOutlet var fgImage: UIImageView!
    @IBOutlet var imgLock: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.layer.cornerRadius = self.contentView.bounds.size.height/2.0
        self.contentView.clipsToBounds = true
        
        self.fgImage.layer.cornerRadius = self.fgImage.bounds.size.height / 2.0
        self.fgImage.clipsToBounds = true
        // Initialization code
    }
    
    public func configure(with model: PatternModel) {
        
        if model.bgName.count > 0 {
            bgImage.image = UIImage(named: model.bgName)
        }
        
        if model.fgName.count > 0 {
            fgImage.image = UIImage(named: model.fgName)
        }
        
    }

}

