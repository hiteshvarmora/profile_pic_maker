//
//  ShadowCollectionCell.swift
//  Profile Picture Maker
//
//  Created by Apple on 03/07/21.
//

import UIKit

class ShadowCollectionCell: UICollectionViewCell {

    @IBOutlet var image: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var viewSelected: UIView!
    @IBOutlet var imgLock: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    public func configure(with model: ShadowModel) {
//        
//        if model.colorName.count > 0 {
//            image.backgroundColor = hexStringToUIColor(hex: model.colorName)
//        }
//    }

}
