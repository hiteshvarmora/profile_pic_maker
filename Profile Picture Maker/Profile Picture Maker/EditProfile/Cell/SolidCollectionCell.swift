//
//  SolidCollectionCell.swift
//  Profile Picture Maker
//
//  Created by Apple on 03/07/21.
//

import UIKit

class SolidCollectionCell: UICollectionViewCell {

    @IBOutlet var image: UIImageView!
    @IBOutlet var imgLock: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    public func configure(with model: SolidModel) {
        image.layer.sublayers?.forEach({ $0.removeFromSuperlayer()})
        image.image = nil
        image.backgroundColor = model.color
    }

    public func configureGradient(with model : GradientModel) {
        image.layer.sublayers?.forEach({ $0.removeFromSuperlayer()})
        image.backgroundColor = nil
        image.image = nil
        
        let gradient = CAGradientLayer()
        gradient.colors = [hexStringToUIColor(hex: model.firstColorHex!).cgColor, hexStringToUIColor(hex: model.secondColorHex!).cgColor]
        gradient.locations = [NSNumber(value: model.locationX!), NSNumber(value: model.locationY!)]
        gradient.frame = image.bounds
        gradient.startPoint = CGPoint(x: model.startPointX!, y: model.startPointY!)
        gradient.endPoint = CGPoint(x: model.endPointX!, y: model.endPointY!)
        
        image.layer.insertSublayer(gradient, at: 0)
    }
    
    public func configureAbstract(with model: AbstractModel) {
        image.layer.sublayers?.forEach({ $0.removeFromSuperlayer()})
        image.backgroundColor = nil

        image.image = UIImage(named: model.imageName)
    }
}

