//
//  GradientModel.swift
//  Profile Picture Maker
//
//  Created by Apple on 04/07/21.
//

import Foundation
import UIKit

struct GradientModel : Codable {
    var firstColorHex : String?
    var secondColorHex : String?
    var locationX : Double?
    var locationY : Double?
    var startPointX : Double?
    var startPointY : Double?
    var endPointX : Double?
    var endPointY : Double?
    
    enum CodingKeys : String, CodingKey {
        case firstColorHex =  "firstColorHex"
        case secondColorHex = "secondColorHex"
        case locationX = "locationX"
        case locationY = "locationY"
        case startPointX = "startPointX"
        case startPointY = "startPointY"
        case endPointX = "endPointX"
        case endPointY = "endPointY"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        firstColorHex = try values.decode(String.self, forKey: .firstColorHex)
        secondColorHex = try values.decode(String.self, forKey: .secondColorHex)
        locationX = try values.decode(Double.self, forKey: .locationX)
        locationY = try values.decode(Double.self, forKey: .locationY)
        startPointX = try values.decode(Double.self, forKey: .startPointX)
        startPointY = try values.decode(Double.self, forKey: .startPointY)
        endPointX = try values.decode(Double.self, forKey: .endPointX)
        endPointY = try values.decode(Double.self, forKey: .endPointY)

    }
}
