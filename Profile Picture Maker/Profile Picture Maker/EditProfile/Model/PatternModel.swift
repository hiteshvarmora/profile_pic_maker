//
//  PatternModel.swift
//  Profile Picture Maker
//
//  Created by Apple on 26/06/21.
//

import Foundation

struct PatternModel {
    var patternId : Int
    var bgName : String
    var fgName : String
}
