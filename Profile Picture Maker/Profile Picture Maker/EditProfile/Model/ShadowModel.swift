//
//  ShadowMode.swift
//  Profile Picture Maker
//
//  Created by Apple on 03/07/21.
//

import Foundation
import UIKit

struct ShadowModel : Codable {
    var title : String?
    var color : String?
    var offsetWidth : Double?
    var offsetHeight : Double?
    var opacity : Double?
    var radius : Double?
    
    enum CodingKeys : String, CodingKey {
        case title =  "title"
        case color = "color"
        case offsetWidth = "offsetWidth"
        case offsetHeight = "offsetHeight"
        case opacity = "opacity"
        case radius = "radius"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decode(String.self, forKey: .title)
        color = try values.decode(String.self, forKey: .color)
        offsetWidth = try values.decode(Double.self, forKey: .offsetWidth)
        offsetHeight = try values.decode(Double.self, forKey: .offsetHeight)
        opacity = try values.decode(Double.self, forKey: .opacity)
        radius = try values.decode(Double.self, forKey: .radius)
    }
}
