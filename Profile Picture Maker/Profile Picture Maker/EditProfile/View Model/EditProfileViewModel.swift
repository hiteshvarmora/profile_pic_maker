//  
//  EditProfileViewModel.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 20/06/21.
//

import Foundation
import UIKit

enum EditOptions {
    case pattern
    case adjust
    case color
    case shadow
}

enum ColorOptions : Int {
    case solid
    case gradient
    case abstract
}

class EditProfileViewModel {

    private var model: [EditProfileModel] = [EditProfileModel]()

    var arrPattern : [PatternModel] = []
    var arrSolidColor : [SolidModel] = []
    var arrGradient : [GradientModel] = []
    var arrAbstract : [AbstractModel] = []
    var arrShadow : [ShadowModel] = []
    var editOption : EditOptions = .pattern
    var colorOption : ColorOptions = .solid
    
    var floatScale : CGFloat?
    var floatRotate : CGFloat?
    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    /// Define selected model
    var selectedObject: EditProfileModel?

    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?
    
    init() {
        for i in 1...32 {
            arrPattern.append(PatternModel(patternId: i, bgName: "bg_\(i)", fgName: "fg_\(i)"))
        }
        
        arrSolidColor.append(SolidModel(color: UIColor(red: 194.0/255.0, green: 199.0/255.0, blue: 214.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 96.0/255.0, green: 246.0/255.0, blue: 173.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 102.0/255.0, green: 124.0/255.0, blue: 137.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 70.0/255.0, green: 229.0/255.0, blue: 209.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 237.0/255.0, green: 99.0/255.0, blue: 56.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 186.0/255.0, green: 224.0/255.0, blue: 255.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 255.0/255.0, green: 65.0/255.0, blue: 108.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 76.0/255.0, green: 137.0/255.0, blue: 248.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 255.0/255.0, green: 137.0/255.0, blue: 0.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 121.0/255.0, green: 69.0/255.0, blue: 234.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 255.0/255.0, green: 228.0/255.0, blue: 0.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 63.0/255.0, green: 43.0/255.0, blue: 150.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 165.0/255.0, green: 234.0/255.0, blue: 32.0/255.0, alpha: 1.0)))
        arrSolidColor.append(SolidModel(color: UIColor(red: 205.0/255.0, green: 90.0/255.0, blue: 236.0/255.0, alpha: 1.0)))
        
        if let fileURL = Bundle.main.url(forResource: "gradients",   withExtension: "plist") {
            do {
                let data = try Data.init(contentsOf: fileURL, options: .mappedIfSafe)
                let decoder = PropertyListDecoder()
                let result = try decoder.decode([GradientModel].self, from: data)
                
                self.arrGradient.append(contentsOf: result)

            } catch {
                print(error.localizedDescription)
            }
        }
        
        for i in 1...21 {
            arrAbstract.append(AbstractModel(imageName: "Abstract_\(i)"))
        }
        
        //Shadow
        if let fileURL = Bundle.main.url(forResource: "shadows",   withExtension: "plist") {
            do {
                let data = try Data.init(contentsOf: fileURL, options: .mappedIfSafe)
                let decoder = PropertyListDecoder()
                let result = try decoder.decode([ShadowModel].self, from: data)
                
                self.arrShadow.append(contentsOf: result)

            } catch {
                print(error.localizedDescription)
            }
        }

    }
    
    func convertPercentage(_ value : Double,_ size : Double) -> Double {
        return (size*value)/100.0
    }

}

extension EditProfileViewModel {

}
