//
//  AdjustView.swift
//  Profile Picture Maker
//
//  Created by Apple on 02/07/21.
//

import UIKit

protocol AdjustViewDelegate: NSObjectProtocol{
    func onZoomChange(_ value : Float)
    func onRotateChange(_ value : Float)
}

class AdjustView : UIView {
    
    @IBOutlet weak var sliderZoom : UISlider!
    @IBOutlet weak var sliderRotate : UISlider!
    
    var delegate: AdjustViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func sliderValueChange(_ slider : UISlider) {
        if slider == sliderZoom {
            delegate?.onZoomChange(slider.value)
        }
        else if slider == sliderRotate {
            delegate?.onRotateChange(slider.value)
        }
    }
}
