//
//  ColorView.swift
//  Profile Picture Maker
//
//  Created by Apple on 03/07/21.
//

import UIKit

protocol ColorViewDelegate: NSObjectProtocol{
    func onSolidColorTapped(_ model : SolidModel?)
    func onGradientTapped(_ model : GradientModel?)
    func onAbstractTapped(_ model : AbstractModel?)
}

class ColorView : UIView {
    
    @IBOutlet weak var collectionSolidColor: UICollectionView!
    @IBOutlet weak var segmentControl : UISegmentedControl!

    var viewModel : EditProfileViewModel!
    var selSolidColor : IndexPath?
    var delegate: ColorViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configureView() {
        
        segmentControl.selectedSegmentIndex = viewModel.colorOption.rawValue
        
        self.collectionSolidColor.delegate = self
        self.collectionSolidColor.dataSource = self
        self.collectionSolidColor.register(UINib.init(nibName: "SolidCollectionCell", bundle: nil), forCellWithReuseIdentifier: "SolidCell")
        self.collectionSolidColor.reloadData()
    }
    
    @IBAction func segmentControlValueChange() {
        selSolidColor = nil
        viewModel.colorOption = ColorOptions(rawValue: segmentControl.selectedSegmentIndex) ?? .solid
        self.collectionSolidColor.reloadData()
    }
}

extension ColorView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch viewModel.colorOption {
        case .solid:
            return viewModel.arrSolidColor.count
        case .gradient:
            return viewModel.arrGradient.count
        case .abstract:
            return viewModel.arrAbstract.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SolidCell", for: indexPath) as! SolidCollectionCell
        
        switch viewModel.colorOption {
        case .solid:
            let model = viewModel.arrSolidColor[indexPath.row]
            cell.configure(with: model)
        case .gradient:
            let model = viewModel.arrGradient[indexPath.row]
            cell.configureGradient(with: model)
        case .abstract:
            let model = viewModel.arrAbstract[indexPath.row]
            cell.configureAbstract(with: model)
        }
        
        if let selIndex = self.selSolidColor, selIndex == indexPath {
            cell.image.layer.borderWidth = 1.3
            cell.image.layer.borderColor = UIColor(named: "clrViolet")?.cgColor
        }
        else{
            cell.image.layer.borderWidth = 0
        }
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 11 {
            cell.imgLock.isHidden = false
        }
        else{
            cell.imgLock.isHidden = true
        }
        
        return cell
    }
}

extension ColorView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selSolidColor = indexPath
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 11 {
            switch viewModel.colorOption {
            case .solid:
                self.delegate?.onSolidColorTapped(nil)
            case .gradient:
                self.delegate?.onGradientTapped(nil)
            case .abstract:
                self.delegate?.onAbstractTapped(nil)
            }
        }
        else{
            switch viewModel.colorOption {
            case .solid:
                self.delegate?.onSolidColorTapped(viewModel.arrSolidColor[indexPath.row])
            case .gradient:
                self.delegate?.onGradientTapped(viewModel.arrGradient[indexPath.row])
            case .abstract:
                self.delegate?.onAbstractTapped(viewModel.arrAbstract[indexPath.row])
            }
        }
        
        
        collectionView.reloadData()
    }
}

extension ColorView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 40, height: 40)
    }
}

