//  
//  EditProfileView.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 20/06/21.
//

import UIKit
import CoreImage

class EditProfileView: UIViewController {

    // OUTLETS HERE
    @IBOutlet weak var imageView : UIImageView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewPreview: UIView!
    @IBOutlet weak var viewWatermark: UIView!
    @IBOutlet weak var viewWatermarkFinal: UIView!
    @IBOutlet weak var imgWatermarkClose: UIImageView!
    @IBOutlet weak var viewBottom: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imgWhite: UIImageView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet weak var imgPattern: UIImageView!
    @IBOutlet weak var imgAdjust: UIImageView!
    @IBOutlet weak var imgColor: UIImageView!
    @IBOutlet weak var imgShadow: UIImageView!
    @IBOutlet weak var lblPattern: UILabel!
    @IBOutlet weak var lblAdjust: UILabel!
    @IBOutlet weak var lblColor: UILabel!
    @IBOutlet weak var lblShadow: UILabel!
    @IBOutlet weak var viewHolder: UIView!
    @IBOutlet weak var viewPattern: UIView!
    @IBOutlet weak var viewAdjust: UIView!
    @IBOutlet weak var viewColor: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var constraintViewHolderHeight: NSLayoutConstraint!
    // VARIABLES HERE
    var viewModel = EditProfileViewModel()
    var image : UIImage?
    var patternView: PatternView?
    var adjustView : AdjustView?
    var colorView : ColorView?
    var shadowView : ShadowView?
    var resizeImg : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.handleContent()
        self.setupViewModel()
        self.configureView()
    }
    
    fileprivate func handleContent() {
        scrollView.contentSize = CGSize(width: 1080.0, height: 1080.0)
        
        let scaleWidth = scrollView.frame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollView.frame.size.height / scrollView.contentSize.height
        let minScale = min(scaleWidth, scaleHeight)
        scrollView.zoomScale = minScale
    }
    
    fileprivate func configureView() {
        self.title = "Edit"
        self.imageView.image = image
        //detect()
        addNavigatioButton()
        showPatternView()
        
        resizeImg = resizeImage(image: image!, targetSize: CGSize(width: 50, height: 70))
        
        viewContent.layer.cornerRadius = 10.0
        viewContent.clipsToBounds = true
        
        viewPreview.layer.cornerRadius = 16.7
        viewPreview.clipsToBounds = true
        
        viewBottom.layer.cornerRadius = 28.3
        viewBottom.clipsToBounds = true
        updateWatermarkView()
        //imgWhite.layer.cornerRadius = imgWhite.bounds.size.height/2.0
        
        viewPattern.addTapGestureRecognizer {
            self.viewModel.editOption = .pattern
            self.updateBottomView()
            self.showPatternView()
        }
        
        viewAdjust.addTapGestureRecognizer {
            self.viewModel.editOption = .adjust
            self.updateBottomView()
            self.showAdjustView()
        }
        
        viewColor.addTapGestureRecognizer {
            self.viewModel.editOption = .color
            self.updateBottomView()
            self.showColorView()
        }
        
        viewShadow.addTapGestureRecognizer {
            self.viewModel.editOption = .shadow
            self.updateBottomView()
            self.showShadowView()
        }
        
        imageView.addPanGestureRecognizer { panGesture in
            let translation = panGesture.translation(in: self.view)
            panGesture.setTranslation(.zero, in: self.view)
            
            self.imageView.center = CGPoint(x: self.imageView.center.x + translation.x, y: self.imageView.center.y + translation.y)

        }
        
        viewPreview.addTapGestureRecognizer {
            DispatchQueue.main.async {
                let img = self.viewContent.asImage()
                
                let model = PreviewViewModel()
                model.img = img
                
                let previewViewVC : PreviewView = StoryBoardName.previewView.instantiateViewController(withIdentifier: "PreviewView") as! PreviewView
                previewViewVC.viewModel = model
                previewViewVC.modalPresentationStyle = .overCurrentContext
                self.navigationController?.present(previewViewVC, animated: true, completion: nil)
            }
        }
        
        viewWatermark.addTapGestureRecognizer {
            DispatchQueue.main.async {
                self.openSalesPage()
            }
        }
        
        refreshPage = {
            self.patternView?.collectionPattern.reloadData()
            self.colorView?.collectionSolidColor.reloadData()
            self.shadowView?.collectionShadow.reloadData()
            self.updateWatermarkView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    fileprivate func updateWatermarkView() {
        if UserDefaults.standard.isSubscribed {
            self.viewWatermark.isHidden = true
        }
        else{
            self.viewWatermark.isHidden = false
        }
    }
    
    fileprivate func setupViewModel() {

        self.viewModel.showAlertClosure = {
            let alert = self.viewModel.alertMessage ?? ""
            print(alert)
        }
        
        self.viewModel.updateLoadingStatus = {
            if self.viewModel.isLoading {
                print("LOADING...")
            } else {
                 print("DATA READY")
            }
        }

        self.viewModel.internetConnectionStatus = {
            print("Internet disconnected")
            // show UI Internet is disconnected
        }

        self.viewModel.serverErrorStatus = {
            print("Server Error / Unknown Error")
            // show UI Server is Error
        }

        self.viewModel.didGetData = {
            // update UI after get data
        }

    }
    
    private func updateBottomView() {
        
        imgPattern.tintColor = UIColor(named: "clrBlack")
        lblPattern.textColor = UIColor(named: "clrBlack")
        imgAdjust.tintColor = UIColor(named: "clrBlack")
        lblAdjust.textColor = UIColor(named: "clrBlack")
        imgColor.tintColor = UIColor(named: "clrBlack")
        lblColor.textColor = UIColor(named: "clrBlack")
        imgShadow.tintColor = UIColor(named: "clrBlack")
        lblShadow.textColor = UIColor(named: "clrBlack")
        
        switch viewModel.editOption {
        case .pattern:
            imgPattern.tintColor = UIColor(named: "clrViolet")
            lblPattern.textColor = UIColor(named: "clrViolet")
        case .adjust:
            imgAdjust.tintColor = UIColor(named: "clrViolet")
            lblAdjust.textColor = UIColor(named: "clrViolet")
        case .color:
            imgColor.tintColor = UIColor(named: "clrViolet")
            lblColor.textColor = UIColor(named: "clrViolet")
        case .shadow:
            imgShadow.tintColor = UIColor(named: "clrViolet")
            lblShadow.textColor = UIColor(named: "clrViolet")
        }
    }
    
    private func addNavigatioButton() {
        
        let btnSave = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(EditProfileView.btnSave(_:)))
        
        if let font = UIFont(name: "Poppins-SemiBold", size: 18.3) {
            btnSave.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        }

        self.navigationItem.rightBarButtonItem = btnSave
    }
    
    @objc func btnSave(_ sender: UIButton) {
        
        DispatchQueue.main.async {
            
            if !self.viewWatermark.isHidden {
                self.viewWatermarkFinal.isHidden = false
            }
            
            let img = self.viewContent.asImage()
            
            let model = ExportProfileViewModel(img)
            
            if !self.viewWatermark.isHidden {
                self.viewWatermarkFinal.isHidden = true
            }
            
            let exportProfileVC : ExportProfileView = StoryBoardName.exportProfile.instantiateViewController(withIdentifier: "ExportProfileView") as! ExportProfileView
            exportProfileVC.viewModel = model
            self.navigationController?.pushViewController(exportProfileVC, animated: true)
        }
    }
    
    private func showPatternView() {
        
        constraintViewHolderHeight.constant = 71
        view.layoutIfNeeded()

        viewHolder.subviews.forEach({ $0.removeFromSuperview() })
        
        if patternView == nil {
            patternView = PatternView.fromNib(named: nil, owner: self)
            patternView?.translatesAutoresizingMaskIntoConstraints = true
            patternView?.frame = viewHolder.bounds
            patternView?.viewModel = viewModel
            patternView?.delegate = self
            patternView?.configureView()
        }
        
        viewHolder.addSubview(patternView!)
    }
    
    private func showAdjustView() {
        
        constraintViewHolderHeight.constant = 80
        view.layoutIfNeeded()

        viewHolder.subviews.forEach({ $0.removeFromSuperview() })
        
        if adjustView == nil {
            adjustView = AdjustView.fromNib(named: nil, owner: self)
            adjustView?.translatesAutoresizingMaskIntoConstraints = true
            adjustView?.frame = viewHolder.bounds
            adjustView?.delegate = self
        }
        
        viewHolder.addSubview(adjustView!)
    }
    
    private func showColorView() {
        
        constraintViewHolderHeight.constant = 150
        view.layoutIfNeeded()
        
        viewHolder.subviews.forEach({ $0.removeFromSuperview() })
        
        if colorView == nil {
            colorView = ColorView.fromNib(named: nil, owner: self)
            colorView?.translatesAutoresizingMaskIntoConstraints = true
            colorView?.frame = viewHolder.bounds
            colorView?.viewModel = viewModel
            colorView?.delegate = self
            colorView?.configureView()
        }
        
        viewHolder.addSubview(colorView!)
    }
    
    private func showShadowView() {
        
        constraintViewHolderHeight.constant = 100
        view.layoutIfNeeded()

        viewHolder.subviews.forEach({ $0.removeFromSuperview() })
        
        if shadowView == nil {
            shadowView = ShadowView.fromNib(named: nil, owner: self)
            shadowView?.translatesAutoresizingMaskIntoConstraints = true
            shadowView?.frame = viewHolder.bounds
            shadowView?.delegate = self
            shadowView?.configureView()
            shadowView?.img = self.resizeImg
            shadowView?.viewModel = viewModel
        }
        
        viewHolder.addSubview(shadowView!)
    }
    
    private func openSalesPage() {
        let salesVC : SalesPageView = StoryBoardName.sales.instantiateViewController(identifier: "SalesPageView") as! SalesPageView
        salesVC.modalPresentationStyle = .fullScreen
        
        self.present(salesVC, animated: true, completion: nil)
    }
}

extension EditProfileView : PatternViewDelegate {
    func onPatternTapped(_ model : PatternModel?) {
        
        DispatchQueue.main.async {
            
            guard let m = model else {
                self.openSalesPage()
                return
            }
            
            self.imgBG.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
            self.imgBG.backgroundColor = nil

            self.imgBG.image = UIImage(named: m.bgName)
            self.imgWhite.image = UIImage(named: m.fgName)
            
        }
    }
}

extension EditProfileView : AdjustViewDelegate {
    func onZoomChange(_ value: Float) {
        DispatchQueue.main.async {
            
            self.viewModel.floatScale = CGFloat(value)
            
            var transform = CGAffineTransform.identity
            transform = transform.scaledBy(x: CGFloat(value), y: CGFloat(value))
            
            if let rotate = self.viewModel.floatRotate {
                transform = transform.rotated(by: rotate)
            }
            
            self.imageView.transform = transform
        }
    }
    
    func onRotateChange(_ value: Float) {
        DispatchQueue.main.async {
            
            var transform = CGAffineTransform.identity
            let degreesToRadians: (CGFloat) -> CGFloat = { (degrees: CGFloat) in
                return degrees / 180.0 * CGFloat.pi
            }
            
            self.viewModel.floatRotate = degreesToRadians(CGFloat(value))
            
            if let scale = self.viewModel.floatScale {
                transform = transform.scaledBy(x: scale, y: scale)
            }

            transform = transform.rotated(by: degreesToRadians(CGFloat(value)))
            self.imageView.transform = transform
        }
    }
}

extension EditProfileView : ColorViewDelegate {
    func onAbstractTapped(_ model: AbstractModel?) {
        DispatchQueue.main.async {
            guard let m = model else {
                self.openSalesPage()
                return
            }
            
            self.imgBG.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
            self.imgBG.backgroundColor = nil
            self.imgBG.image = UIImage(named: m.imageName)
        }
    }
    
    func onGradientTapped(_ model: GradientModel?) {
        DispatchQueue.main.async {
            guard let m = model else {
                self.openSalesPage()
                return
            }
            
            self.imgBG.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
            self.imgBG.image = nil
            self.imgBG.backgroundColor = nil
            
            let gradient = CAGradientLayer()
            gradient.colors = [hexStringToUIColor(hex: m.firstColorHex!).cgColor, hexStringToUIColor(hex: m.secondColorHex!).cgColor]
            gradient.locations = [NSNumber(value: m.locationX!), NSNumber(value: m.locationY!)]
            gradient.frame = self.imgBG.bounds
            gradient.startPoint = CGPoint(x: m.startPointX!, y: m.startPointY!)
            gradient.endPoint = CGPoint(x: m.endPointX!, y: m.endPointY!)
            
            self.imgBG.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    func onSolidColorTapped(_ model : SolidModel?) {
        DispatchQueue.main.async {
            guard let m = model else {
                self.openSalesPage()
                return
            }
            self.imgBG.layer.sublayers?.forEach({ $0.removeFromSuperlayer() })
            self.imgBG.image = nil
            self.imgBG.backgroundColor = m.color
        }
    }
}

extension EditProfileView : ShadowViewDelegate {
    func onShadowTapped(_ model : ShadowModel?) {
        guard let m = model else {
            self.openSalesPage()
            return
        }
        
        imageView.layer.shadowColor = hexStringToUIColor(hex: m.color!).cgColor
        imageView.layer.shadowOffset = CGSize(width: viewModel.convertPercentage(m.offsetWidth!, Double(imageView.bounds.size.width)), height: viewModel.convertPercentage(m.offsetHeight!, Double(imageView.bounds.size.height)))
        imageView.layer.shadowOpacity = Float(m.opacity!)
        imageView.layer.shadowRadius = CGFloat(m.radius!)
        imageView.clipsToBounds = false
    }
}

extension EditProfileView {
    private func detect() {
        
        guard let personciImage = CIImage(image: imageView.image!) else {
            return
        }

        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage)

        // For converting the Core Image Coordinates to UIView Coordinates
        let ciImageSize = personciImage.extent.size
        var transform = CGAffineTransform(scaleX: 1, y: -1)
        transform = transform.translatedBy(x: 0, y: -ciImageSize.height)
        
        for face in faces as! [CIFaceFeature] {
            
            print("Found bounds are \(face.bounds)")
            
            // Apply the transform to convert the coordinates
            var faceViewBounds = face.bounds.applying(transform)
            
            // Calculate the actual position and size of the rectangle in the image view
            let viewSize = imageView.bounds.size
            let scale = min(viewSize.width / ciImageSize.width,
                            viewSize.height / ciImageSize.height)
            let offsetX = (viewSize.width - ciImageSize.width * scale) / 2
            let offsetY = (viewSize.height - ciImageSize.height * scale) / 2
            
            faceViewBounds = faceViewBounds.applying(CGAffineTransform(scaleX: scale, y: scale))
            faceViewBounds.origin.x += offsetX
            faceViewBounds.origin.y += offsetY
            
            let faceBox = UIView(frame: faceViewBounds)

            faceBox.layer.borderWidth = 3
            faceBox.layer.borderColor = UIColor.red.cgColor
            faceBox.backgroundColor = UIColor.clear
            imageView.addSubview(faceBox)
            
            if face.hasLeftEyePosition {
                print("Left eye bounds are \(face.leftEyePosition)")
            }
            
            if face.hasRightEyePosition {
                print("Right eye bounds are \(face.rightEyePosition)")
            }
        }
    }
}
