//
//  PatternView.swift
//  Profile Picture Maker
//
//  Created by Apple on 02/07/21.
//

import UIKit

protocol PatternViewDelegate: NSObjectProtocol{
    func onPatternTapped(_ model : PatternModel?)
}

class PatternView: UIView {
    @IBOutlet weak var collectionPattern: UICollectionView!

    var viewModel : EditProfileViewModel!
    var selPattern : IndexPath?
    var delegate: PatternViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configureView() {
        self.collectionPattern.delegate = self
        self.collectionPattern.dataSource = self
        self.collectionPattern.register(UINib.init(nibName: "PatternCollectionCell", bundle: nil), forCellWithReuseIdentifier: "PatternCell")
        self.collectionPattern.reloadData()
        
        self.selPattern = IndexPath(row: 0, section: 0)
        self.delegate?.onPatternTapped(viewModel.arrPattern[0])
    }
}

extension PatternView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.arrPattern.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PatternCell", for: indexPath) as! PatternCollectionCell
        
        let model = viewModel.arrPattern[indexPath.row]
        
        cell.configure(with: model)
        
        if let selIndex = self.selPattern, selIndex == indexPath {
            cell.contentView.layer.borderWidth = 3.3
            cell.contentView.layer.borderColor = UIColor(named: "clrViolet")?.cgColor
        }
        else{
            cell.contentView.layer.borderWidth = 0
        }
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 11 {
            cell.imgLock.isHidden = false
        }
        else{
            cell.imgLock.isHidden = true
        }
        
        return cell
    }
}

extension PatternView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selPattern = indexPath
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 11 {
            self.delegate?.onPatternTapped(nil)
        }
        else{
            self.delegate?.onPatternTapped(viewModel.arrPattern[indexPath.row])
        }
        
        collectionView.reloadData()
    }
}

extension PatternView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 71, height: 71)
    }
}

