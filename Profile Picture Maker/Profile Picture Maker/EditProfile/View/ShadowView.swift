//
//  ShadowView.swift
//  Profile Picture Maker
//
//  Created by Apple on 03/07/21.
//

import UIKit

protocol ShadowViewDelegate: NSObjectProtocol{
    func onShadowTapped(_ model : ShadowModel?)
}

class ShadowView: UIView {
    @IBOutlet weak var collectionShadow: UICollectionView!

    var viewModel : EditProfileViewModel!
    var selShadow : IndexPath?
    var delegate: ShadowViewDelegate?
    var img : UIImage?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    func configureView() {
        self.collectionShadow.delegate = self
        self.collectionShadow.dataSource = self
        self.collectionShadow.register(UINib.init(nibName: "ShadowCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ShadowCell")
        self.collectionShadow.reloadData()
    }
}

extension ShadowView : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.arrShadow.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShadowCell", for: indexPath) as! ShadowCollectionCell
        
        let model = viewModel.arrShadow[indexPath.row]
        
        cell.image.image = self.img
        cell.lblTitle.text = model.title ?? "Shadow"
        
        cell.image.layer.shadowColor = hexStringToUIColor(hex: model.color!).cgColor
        cell.image.layer.shadowOffset = CGSize(width: viewModel.convertPercentage(model.offsetWidth!, Double(cell.image.bounds.size.width)), height: viewModel.convertPercentage(model.offsetHeight!, Double(cell.image.bounds.size.height)))
        cell.image.layer.shadowOpacity = Float(model.opacity!)
        cell.image.layer.shadowRadius = CGFloat(model.radius!)
        cell.image.clipsToBounds = false

        if let selIndex = self.selShadow, selIndex == indexPath {
            cell.viewSelected.isHidden = false
            cell.lblTitle.textColor = UIColor(named: "clrViolet")
        }
        else{
            cell.viewSelected.isHidden = true
            cell.lblTitle.textColor = UIColor(named: "clrBlack")
        }
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 3 {
            cell.imgLock.isHidden = false
        }
        else{
            cell.imgLock.isHidden = true
        }
        
        return cell
    }
}

extension ShadowView : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.selShadow = indexPath
        
        if !UserDefaults.standard.isSubscribed && indexPath.row > 3 {
            self.delegate?.onShadowTapped(nil)
        }
        else{
            self.delegate?.onShadowTapped(viewModel.arrShadow[indexPath.row])
        }
        
        collectionView.reloadData()
    }
}

extension ShadowView : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 50, height: 90)
    }
}

