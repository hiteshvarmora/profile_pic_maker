//  
//  ExportProfileViewModel.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 20/06/21.
//

import Foundation
import UIKit

class ExportProfileViewModel {

    var img : UIImage!
    
    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?

    init(_ img: UIImage) {
        self.img = img
    }

}

extension ExportProfileViewModel {

}
