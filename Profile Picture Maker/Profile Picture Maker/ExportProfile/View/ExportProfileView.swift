//  
//  ExportProfileView.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 20/06/21.
//

import UIKit
import StoreKit
import Toaster

class ExportProfileView: UIViewController {

    // OUTLETS HERE
    @IBOutlet weak var imageView: UIImageView!
    var text: String = ""
    // VARIABLES HERE
    var viewModel : ExportProfileViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewModel()
        imageView.image = viewModel.img
        
        self.addRightBarButton()
        
        if let img = viewModel.img {
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
        
        SKStoreReviewController.requestReview()
        //AppStoreReviewManager.requestReviewIfAppropriate()
    }
    
    fileprivate func setupViewModel() {
        

    }
    
    fileprivate func addRightBarButton() {
        
        let btnSave = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(ExportProfileView.backToHome))
        
        if let font = UIFont(name: "Poppins-SemiBold", size: 18.3) {
            btnSave.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
        }

        self.navigationItem.rightBarButtonItem = btnSave
    }
    
    @objc fileprivate func backToHome() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnsaveTapped(_ sender: UIButton) {
        if let img = imageView.image {
            UIImageWriteToSavedPhotosAlbum(img, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        }
    }
    
    @IBAction func btnWhatsAppClick(_ sender: UIButton) {
        
        shareImage()
    }
    
    @IBAction func btnInstagramClick(_ sender: UIButton) {
        shareImage()
    }
    
    @IBAction func btnShareClick(_ sender: UIButton) {
        shareImage()
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            //self.showAlert("Failed", message: error.localizedDescription)
            Toast(text: error.localizedDescription).show()
        }
        else {
            Toast(text: "The image is saved into your Photo Library.").show()
            //self.showAlert("Image saved", message: "The image is saved into your Photo Library.")
        }
    }
 
    private func showAlert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            DispatchQueue.main.async {
                SKStoreReviewController.requestReview()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func shareImage() {
        let imageToShare = [ viewModel.img ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare as [Any], applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = []
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
