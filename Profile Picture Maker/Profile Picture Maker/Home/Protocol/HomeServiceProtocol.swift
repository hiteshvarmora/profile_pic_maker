//  
//  HomeServiceProtocol.swift
//  Profile Picture Maker
//
//  Created by Apple on 15/06/21.
//

import Foundation

protocol HomeServiceProtocol {

    /// SAMPLE FUNCTION -* Please rename this function to your real function
    ///
    /// - Parameters:
    ///   - success: -- success closure response, add your Model on this closure.
    ///                 example: success(_ data: YourModelName) -> ()
    ///   - failure: -- failure closure response, add your Model on this closure.  
    ///                 example: success(_ data: APIError) -> ()
    func removeThisFuncName(success: @escaping(_ data: HomeModel) -> (), failure: @escaping() -> ())

}
