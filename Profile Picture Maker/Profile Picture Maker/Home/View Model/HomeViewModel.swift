//  
//  HomeViewModel.swift
//  Profile Picture Maker
//
//  Created by Apple on 15/06/21.
//

import Foundation

class HomeViewModel {

    private let service: HomeServiceProtocol

    private var model: [HomeModel] = [HomeModel]() {
        didSet {
            self.count = self.model.count
        }
    }

    /// Count your data in model
    var count: Int = 0

    //MARK: -- Network checking

    /// Define boolean for internet status, call when network disconnected
    var isDisconnected: Bool = false {
        didSet {
            self.alertMessage = "No network connection. Please connect to the internet"
            self.internetConnectionStatus?()
        }
    }

    //MARK: -- UI Status

    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    /// Define selected model
    var selectedObject: HomeModel?

    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?

    init(withHome serviceProtocol: HomeServiceProtocol = HomeService() ) {
        self.service = serviceProtocol

    }

    //MARK: Internet monitor status
    @objc func networkStatusChanged(_ notification: Notification) {
        
    }
}

extension HomeViewModel {

}
