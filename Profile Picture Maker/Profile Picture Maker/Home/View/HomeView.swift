//  
//  HomeView.swift
//  Profile Picture Maker
//
//  Created by Apple on 15/06/21.
//

import UIKit

class HomeView: UIViewController {

    // OUTLETS HERE

    // VARIABLES HERE
    var viewModel = HomeViewModel()

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !UserDefaults.standard.isOnboardingShown {
            showOnboardingViewController()
        }
//        else if !UserDefaults.standard.isSubscribed {
//            openSalesPage()
//        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(openSalesPage), name: NSNotification.Name.init("inAppPricesUpdated"), object: nil)

        self.setupViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    fileprivate func setupViewModel() {
        
    }
    
    @IBAction func btnChoosePhotoClick(_ sender: UIButton) {
        let chooseProfileVC:ChooseProfileView = StoryBoardName.chooseProfile.instantiateViewController(withIdentifier: "ChooseProfileView") as! ChooseProfileView
        self.navigationController?.pushViewController(chooseProfileVC, animated: true)
    }
    
    func showOnboardingViewController() {
        let chooseOnboardingVC = StoryBoardName.introScreen.instantiateViewController(withIdentifier: "IntroNavigation") as! UINavigationController
        chooseOnboardingVC.modalPresentationStyle = .fullScreen
        self.navigationController?.present(chooseOnboardingVC, animated: false, completion: nil)
    }
    
    @objc private func openSalesPage() {
        if !UserDefaults.standard.isSubscribed && UserDefaults.standard.isOnboardingShown {
            let salesVC : SalesPageView = StoryBoardName.sales.instantiateViewController(identifier: "SalesPageView") as! SalesPageView
            salesVC.modalPresentationStyle = .fullScreen
            
            self.present(salesVC, animated: true, completion: nil)
        }
        
    }

}


