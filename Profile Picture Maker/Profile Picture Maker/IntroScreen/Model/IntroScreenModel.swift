//  
//  IntroScreenModel.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 28/06/21.
//

import Foundation
import UIKit

struct IntroScreenModel {
    var imageName       : String
    var title           : String
    var description     : String
    var backgroundColor : UIColor
}
