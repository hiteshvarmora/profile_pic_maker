//  
//  IntroScreenViewModel.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 28/06/21.
//

import Foundation

class IntroScreenViewModel {

    var arrIntroData: [IntroScreenModel] = [IntroScreenModel]() {
        didSet {
            self.count = self.arrIntroData.count
        }
    }
    
    /// Count your data in model
    var count: Int = 0

    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?

    init() {
        arrIntroData.append(IntroScreenModel(imageName: ConstantsPP.kIntroImage_1, title: ConstantsPP.kIntroTitle_1, description: ConstantsPP.kIntroDescription_1, backgroundColor: ConstantsPP.kIntroBackgroundColor_1))
        arrIntroData.append(IntroScreenModel(imageName: ConstantsPP.kIntroImage_2, title: ConstantsPP.kIntroTitle_2, description: ConstantsPP.kIntroDescription_2, backgroundColor: ConstantsPP.kIntroBackgroundColor_2))
        arrIntroData.append(IntroScreenModel(imageName: ConstantsPP.kIntroImage_3, title: ConstantsPP.kIntroTitle_3, description: ConstantsPP.kIntroDescription_3, backgroundColor: ConstantsPP.kIntroBackgroundColor_3))
        
    }

}

extension IntroScreenViewModel {

}
