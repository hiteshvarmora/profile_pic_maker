//
//  IntroCell.swift
//  IndoKiwiGrocers
//
//  Created by Nirav Gondaliya on 20/02/19.
//  Copyright © 2019 IndoKiwiGrocers. All rights reserved.
//

import UIKit

class IntroCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var vwBack: UIView!
    @IBOutlet weak var viewAnimation: UIView!
    @IBOutlet weak var imgBG: UIImageView!
    @IBOutlet var imgIntro: UIImageView!
    
    @IBOutlet weak var pageControll: UIPageControl!
    
    @IBOutlet var constImageHeight: NSLayoutConstraint!
    @IBOutlet var constImageWidth: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
