//  
//  IntroScreenView.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 28/06/21.
//

import UIKit

class IntroScreenView: UIViewController {

    // OUTLETS HERE
    @IBOutlet weak var clsIntro: UICollectionView!

    @IBOutlet weak var btnNext: UIButton!
    
    // VARIABLES HERE
    var viewModel = IntroScreenViewModel()
    var currentIndex = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clsIntro.register(UINib (nibName: ConstantsPP.kIntroCell, bundle: nil), forCellWithReuseIdentifier: ConstantsPP.kIntroCell)
        
        self.setupViewModel()
    }
    
    fileprivate func setupViewModel() {
        
        self.clsIntro.delegate = self
        self.clsIntro.dataSource = self
        
        self.clsIntro.reloadData()
        
    }
    
    @IBAction func btnNextClick(_ sender: UIButton) {
        if currentIndex == 2 {
            UserDefaults.standard.isOnboardingShown = true
            let salesVC : SalesPageView = StoryBoardName.sales.instantiateViewController(identifier: "SalesPageView") as! SalesPageView
            self.navigationController?.pushViewController(salesVC, animated: true)

        } else {
            //let cellSize = clsIntro.frame.size
            //let contentOffset = self.clsIntro.contentOffset
            
            //let height = UIScreen.main.bounds.height
            //self.clsIntro.scrollRectToVisible(CGRect (x: contentOffset.x + cellSize.width, y: 0, width: cellSize.width, height: height), animated: true)
            
            let collectionBounds = self.clsIntro.bounds
            let contentOffset = CGFloat(floor(self.clsIntro.contentOffset.x + collectionBounds.size.width))
            self.moveCollectionToFrame(contentOffset: contentOffset)
            
        }
    }
    
    
}

//MARK: - UICollectionViewDelegate & UICollectionViewDataSource Methods
extension IntroScreenView : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.arrIntroData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        
        let introData = self.viewModel.arrIntroData[indexPath.row]
        
        //cell.imgBG.image = UIImage(named: introData.imageName)
        
        cell.lblTitle.text = introData.title
        cell.lblDescription.text = introData.description
        
        cell.lblTitle.textColor = #colorLiteral(red: 0.2078431373, green: 0.2274509804, blue: 0.3137254902, alpha: 1)
        cell.lblDescription.textColor = #colorLiteral(red: 0.2078431373, green: 0.2274509804, blue: 0.3137254902, alpha: 1)
        
        cell.imgIntro.image = UIImage(named: introData.imageName)
        
        cell.vwBack.backgroundColor = introData.backgroundColor
        
//        cell.constImageHeight.constant = 394 //cell.imgIntro.frame.width + 100
//        cell.constImageWidth.constant = 345 //cell.im
        //UIApplication.shared.statusBarView?.backgroundColor = introData.backgroundColor
        
        cell.pageControll.currentPage = indexPath.row
        
        return cell
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x: contentOffset, y: self.clsIntro.contentOffset.y, width: self.clsIntro.frame.width, height: self.clsIntro.frame.height)
        self.clsIntro.scrollRectToVisible(frame, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.clsIntro.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        currentIndex = indexPath.row
        /*
        if indexPath.row == 0 {
            UIApplication.shared.statusBarView?.backgroundColor = self.arrIntroData[0].backgroundColor
        } else if indexPath.row == 1 {
            UIApplication.shared.statusBarView?.backgroundColor = self.arrIntroData[1].backgroundColor
        } else if indexPath.row == 2 {
            UIApplication.shared.statusBarView?.backgroundColor = self.arrIntroData[2].backgroundColor
        } else if indexPath.row == 3 {
            UIApplication.shared.statusBarView?.backgroundColor = self.arrIntroData[3].backgroundColor
        } */
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //self.pageControll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        //self.updatePageController()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        //self.pageControll.currentPage = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
        //self.updatePageController()
    }
    
    func updatePageController() {
        
    }
}



