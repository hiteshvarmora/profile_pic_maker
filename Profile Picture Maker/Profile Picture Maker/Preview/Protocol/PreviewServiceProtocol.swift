//  
//  PreviewServiceProtocol.swift
//  Profile Picture Maker
//
//  Created by Apple on 11/07/21.
//

import Foundation

protocol PreviewServiceProtocol {

    /// SAMPLE FUNCTION -* Please rename this function to your real function
    ///
    /// - Parameters:
    ///   - success: -- success closure response, add your Model on this closure.
    ///                 example: success(_ data: YourModelName) -> ()
    ///   - failure: -- failure closure response, add your Model on this closure.  
    ///                 example: success(_ data: APIError) -> ()
    func removeThisFuncName(success: @escaping(_ data: PreviewModel) -> (), failure: @escaping() -> ())

}
