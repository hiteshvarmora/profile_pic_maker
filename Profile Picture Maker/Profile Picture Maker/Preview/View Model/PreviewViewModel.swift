//  
//  PreviewViewModel.swift
//  Profile Picture Maker
//
//  Created by Apple on 11/07/21.
//

import Foundation
import UIKit

class PreviewViewModel {

    private var model: [PreviewModel] = [PreviewModel]()

    var img: UIImage?
    
    //MARK: -- UI Status

    /// Update the loading status, use HUD or Activity Indicator UI
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }

    /// Showing alert message, use UIAlertController or other Library
    var alertMessage: String? {
        didSet {
            self.showAlertClosure?()
        }
    }

    /// Define selected model
    var selectedObject: PreviewModel?

    //MARK: -- Closure Collection
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var internetConnectionStatus: (() -> ())?
    var serverErrorStatus: (() -> ())?
    var didGetData: (() -> ())?

}

extension PreviewViewModel {

}
