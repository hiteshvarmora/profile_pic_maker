//  
//  PreviewView.swift
//  Profile Picture Maker
//
//  Created by Apple on 11/07/21.
//

import UIKit

class PreviewView: UIViewController {

    // OUTLETS HERE
    @IBOutlet weak var imgPreview: UIImageView!

    // VARIABLES HERE
    var viewModel = PreviewViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewModel()
        self.imgPreview.image = viewModel.img ?? nil
    }
    
    fileprivate func setupViewModel() {

        self.viewModel.showAlertClosure = {
            let alert = self.viewModel.alertMessage ?? ""
            print(alert)
        }
        
        self.viewModel.updateLoadingStatus = {
            if self.viewModel.isLoading {
                print("LOADING...")
            } else {
                 print("DATA READY")
            }
        }

        self.viewModel.internetConnectionStatus = {
            print("Internet disconnected")
            // show UI Internet is disconnected
        }

        self.viewModel.serverErrorStatus = {
            print("Server Error / Unknown Error")
            // show UI Server is Error
        }

        self.viewModel.didGetData = {
            // update UI after get data
        }

    }
    
    @IBAction func btnDone(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}


