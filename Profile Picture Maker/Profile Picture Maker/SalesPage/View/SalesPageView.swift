//  
//  SalesPageView.swift
//  Profile Picture Maker
//
//  Created by Parth Patel on 29/06/21.
//

import UIKit
import AVKit
import IHProgressHUD

class SalesPageView: UIViewController {

    // OUTLETS HERE
    @IBOutlet var viewSalesVideo: UIView!
    @IBOutlet var lblYearlyPrice: UILabel!
    @IBOutlet var lblWeeklyPrice: UILabel!
    @IBOutlet var continueView : UIView!
    @IBOutlet var weeklyView : UIView!
    @IBOutlet var yearlyView: UIView!
    @IBOutlet var yearlyImage: UIImageView!
    @IBOutlet var weeklyImage: UIImageView!
    
    // VARIABLES HERE
    var viewModel = SalesPageViewModel()
    var player: AVPlayer!
    
    var index = 0
    var weeklyPrice : String = ""
    var yearlyPrice : String = ""
    var isWeekly : Bool = false
    
    var isFromAppStore : Bool = false
    var isFromShareScreen : Bool = false
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViewModel()
        self.configView()
    }
    
    fileprivate func setupViewModel() {
        
    }
    
    func configView() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(updatePrices), name: NSNotification.Name.init("inAppPricesUpdated"), object: nil)

        
        let formattedPrices = AppUtil.arrPriceIAPLocale.getFormattedPrices()
        
        weeklyPrice = formattedPrices.weeklyPrice == "" ? "$2.99" : formattedPrices.weeklyPrice
        yearlyPrice = formattedPrices.monthlyPrice == "" ? "$29.99" : formattedPrices.monthlyPrice
        
        self.lblWeeklyPrice.text = "\(weeklyPrice)/week"
        self.lblYearlyPrice.text = "\(yearlyPrice)/year"
        
        selectWeekly(isWeekly: isWeekly)
    }
    
    @objc func updatePrices() {
        let formattedPrices = AppUtil.arrPriceIAPLocale.getFormattedPrices()
        
        weeklyPrice = formattedPrices.weeklyPrice == "" ? "$2.99" : formattedPrices.weeklyPrice
        yearlyPrice = formattedPrices.monthlyPrice == "" ? "$29.99" : formattedPrices.monthlyPrice
        
        self.lblWeeklyPrice.text = "\(weeklyPrice)/week"
        self.lblYearlyPrice.text = "\(yearlyPrice)/year"

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard let path = Bundle.main.path(forResource: "Sales-page-video-compressed", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }

        player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = viewSalesVideo.bounds //CGRect(x: 0, y: 0, width: viewSalesVideo.frame.width, height: viewSalesVideo.frame.height)

        viewSalesVideo.layer.addSublayer(playerLayer)
        player.play()
        
        viewSalesVideo.backgroundColor = .yellow
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
            self?.player?.seek(to: CMTime.zero)
            self?.player?.play()
        }

    }

    //MARK: - Button Actions
    @IBAction func closeAction(_ sender: Any) {
        if(self.isFromShareScreen == true)
        {
            dismiss(animated: true)
        }
        else {
            DispatchQueue.main.async {
                if let presentViewController = self.presentedViewController {
                    presentViewController.presentingViewController?.dismiss(animated: true)
                }else {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func termsAction() {
        guard let url = URL(string: SalesData.TermsOfService) else {
          return //be safe
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func privacyAction() {
        guard let url = URL(string: SalesData.PrivacyPolicy) else {
          return //be safe
        }

        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    @IBAction func restoreAction(_ sender: Any) {
        IHProgressHUD.set(font: UIFont.init(name: "Poppins-Bold", size: 12)!)
        IHProgressHUD.show(withStatus: "Restoring Purchases")
        InAppShared.shared.restoreTransaction(isRevenueCat:Constants.appDelegate.isRevenuecat,completion: { isSuccess, msg  in
            if isSuccess {
                UserDefaults.standard.isSubscribed = true
                DispatchQueue.main.async {
                    AppDelegate.sharedInstance().configureStoreForRestore(isRevenueCat: Constants.appDelegate.isRevenuecat) {
                        IHProgressHUD.dismiss()
                        if !UserDefaults.standard.isSubscribed{
                            let objAlertController = UIAlertController.init(title: "Nothing to restore", message: "Your purchase may be expired.Please resubscribe to use pro feature", preferredStyle: .alert)
                            let aAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                            objAlertController.addAction(aAction)
                            self.present(objAlertController, animated: true, completion: nil)
                        }
                        else{
                            self.dismissSalesPage(true)
                        }
                    }
                }
                
            } else {
                IHProgressHUD.dismiss()
                let objAlertController = UIAlertController.init(title: "Restore Transactions Failed", message: msg, preferredStyle: .alert)
                let aAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
                objAlertController.addAction(aAction)
                self.present(objAlertController, animated: true, completion: nil)
            }
        })
    }
    
    @IBAction func btnSubscribeClicked(_ sender: UIButton) {
        if sender.tag == 100 { //btnweekly
            isWeekly = true
        } else {
            isWeekly = false
        }
        selectWeekly(isWeekly: isWeekly)
    }
    
    @IBAction func continueAction(_ sender: Any) {
        UIButton.animate(withDuration: 0.2, animations: {
            self.continueView.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        },completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
                self.continueView.transform = CGAffineTransform.identity
            })
        })
        
        let prices = AppUtil.arrPriceIAPLocale.getPrices()
        let introductoryPrices = AppUtil.arrPriceIAPLocale.getIntroductoryPrices()
        let currencyCode = AppUtil.arrPriceIAPLocale.getCurrencyCode()
        
        var price : String = "0"
        if  isWeekly {
            InAppShared.shared.currentID = WEEKLY_SUBSCRIPTION_PRODUCT_ID
            price = prices.weeklyPrice
        }
        else{
            InAppShared.shared.currentID = YEARLY_SUBSCRIPTION_PRODUCT_ID
            price = prices.monthlyPrice
        }
        
        InAppShared.shared.currentPrice =  price
        IHProgressHUD.show()
        InAppShared.shared.purchase(isRevenueCat: Constants.appDelegate.isRevenuecat, completion: { product, transaction, isSuccess in
            IHProgressHUD.dismiss()
            
            AppDelegate.sharedInstance().configureStore(InAppShared.shared.currentID)
                        
            UserDefaults.standard.isSubscribed = true
            self.dismissSalesPage()
            
        }, errorBlock: { msg in
            IHProgressHUD.dismiss()
            let objAlertController = UIAlertController.init(title: "Payment Transaction Failed", message: msg, preferredStyle: .alert)
            let aAction = UIAlertAction.init(title: "OK", style: .default, handler: nil)
            objAlertController.addAction(aAction)
            self.present(objAlertController, animated: true, completion: nil)
        })
    }
    
    func selectWeekly(isWeekly : Bool) {
        self.isWeekly = isWeekly
        
        if isWeekly {
            weeklyView.backgroundColor = UIColor(named: "clrOffWhite")
            weeklyImage.image = UIImage(named: "ic_selected")
            yearlyView.backgroundColor = UIColor.clear
            yearlyImage.image = UIImage(named: "ic_unselected")
        }
        else{
            weeklyView.backgroundColor = UIColor.clear
            weeklyImage.image = UIImage(named: "ic_unselected")
            yearlyView.backgroundColor = UIColor(named: "clrOffWhite")
            yearlyImage.image = UIImage(named: "ic_selected")
        }
    }

    func dismissSalesPage(_ isRestore: Bool = false)
    {
        if let block = refreshPage {
            block()
        }
        if let presentViewController = self.presentedViewController {
            if(isRestore)
            {
                presentViewController.presentingViewController?.dismiss(animated: true, completion: {
                    Profile_Pic_Maker.showAlert("Restore purchase successful")
                })
//                if !UserDefaults.standard.isOnboardingShown {
//                    self.setRootViewController()
//                }else {
                    if let block = refreshPage {
                        block()
                    }
//                }
                return
            }
            presentViewController.presentingViewController?.dismiss(animated: true)
        }else {
            if(isRestore)
            {
                self.dismiss(animated: true) {
                    Profile_Pic_Maker.showAlert("Restore purchase successful")
                }
                if !UserDefaults.standard.isOnboardingShown {
                    AppDelegate.sharedInstance().setRootViewController()
                }else {
                    if let block = refreshPage {
                        block()
                    }
                }
                return
            }
            self.dismiss(animated: true)
        }
        if !UserDefaults.standard.isOnboardingShown {
            AppDelegate.sharedInstance().setRootViewController()
        }else {
            if let block = refreshPage {
                block()
            }
        }
    }

}
