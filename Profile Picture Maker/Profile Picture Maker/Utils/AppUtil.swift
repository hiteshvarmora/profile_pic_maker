//
//  AppUtil.swift
//  Poster
//
//  Created by SOTSYS220 on 08/09/18.
//  Copyright © 2018 Dave. All rights reserved.
//

import UIKit

typealias AlertButtonPressBlock = (_ buttonTitle : String) -> (Void)?
var refreshPage:(() -> ())?

let APPNAME = "Thumbnail Maker"
let WEEKLY_SUBSCRIPTION_PRODUCT_ID = "com.ankit.ProfilePictureMaker.weekly"
let YEARLY_SUBSCRIPTION_PRODUCT_ID = "com.ankit.ProfilePictureMaker.yearly"

let PRIVACY_POLICY_URL = ""
let TERMS_OF_USE_URL = ""

let IN_APP_SHARED_SECRET = "bb005d0bee73462da44222438de1c39b"

class AppUtil: NSObject {
    
    static var instance: AppUtil!
    static var arrPriceIAPLocale : [InAppPriceModel] = []

    // SHARED INSTANCE
    class func sharedInstance() -> AppUtil {
        self.instance = (self.instance ?? AppUtil())
        return self.instance
    }
    
    //MARK: - Alert
    
    var alerButtonPressBlock : AlertButtonPressBlock? = nil
    
    func setAlertButtonPressBlock(block : @escaping AlertButtonPressBlock){
        self.alerButtonPressBlock = block;
    }
    
    func showalertwithTwoButtonAction(_ msg : String,btnTitleOK : String,btnTitleCancel : String,vc : UIViewController)
    {
        let promptController = UIAlertController(title: APPNAME, message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: btnTitleOK, style: .default) { (action) -> Void in
            if let buttonBlock = self.alerButtonPressBlock{
                
                buttonBlock(btnTitleOK)
            }else {
            }
            
        }
        let cancel = UIAlertAction(title: btnTitleCancel, style: .cancel) { (action) -> Void in
            if let buttonBlock = self.alerButtonPressBlock{
                buttonBlock(btnTitleCancel);
            }else {
            }
        }
        promptController.addAction(ok)
        promptController.addAction(cancel)
        vc.present(promptController, animated: true, completion: nil)
    }
}
