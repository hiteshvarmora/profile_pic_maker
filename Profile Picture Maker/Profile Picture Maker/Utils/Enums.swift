//
//  Enums.swift
//  Thumbnail Maker
//
//  Created by SOTSYS104 on 30/04/19.
//  Copyright © 2019 SOTSYS104. All rights reserved.
//

import UIKit

enum MediaType {
    case Camera, Gallery
}

enum AnimationType {
    case top, bottom
}

enum UndoRedoType {
    case textChange, textFont, textFontSize, textColor, textOpacity, textNew, objectTransform, objectFlip, objectMove, objectNew, objectDelete, filter, backGroundChange
}

enum TemplateTypeFirebase:String {
    case channelArt = "Channel Art"
    case thumbnail = "Thumbnail"
}
