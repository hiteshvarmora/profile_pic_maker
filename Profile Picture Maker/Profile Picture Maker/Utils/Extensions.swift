//
//  Extensions.swift
//  Thumbnail Maker
//
//  Created by SOTSYS104 on 27/04/19.
//  Copyright © 2019 SOTSYS104. All rights reserved.
//

import UIKit
import SDWebImagePDFCoder
import CoreML

extension UIView
{
    @IBInspectable
    public var cornerRadius: CGFloat
    {
        set (radius) {
            self.layer.cornerRadius = radius
            self.layer.masksToBounds = radius > 0
        }
        
        get {
            return self.layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat
    {
        set (borderWidth) {
            self.layer.borderWidth = borderWidth
        }
        
        get {
            return self.layer.borderWidth
        }
    }
    
    @IBInspectable
    public var borderColor:UIColor?
    {
        set (color) {
            self.layer.borderColor = color?.cgColor
        }
        
        get {
            if let color = self.layer.borderColor
            {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable
    public var shadowColor:UIColor?
    {
        set (color) {
            self.layer.shadowColor = color?.cgColor
        }
        get {
            if let color = self.layer.shadowColor
            {
                return UIColor(cgColor: color)
            } else {
                return nil
            }
        }
    }
    
    @IBInspectable
    public var shadowRadius: CGFloat
    {
        set (shadowRadius) {
            self.layer.shadowRadius = shadowRadius
        }
        
        get {
            return self.layer.shadowRadius
        }
    }
    
    @IBInspectable
    public var shadowOffset: CGSize {
        set {
            layer.shadowOffset = newValue
        }
        get {
            return layer.shadowOffset
        }
    }
    
    @IBInspectable
    public var shadowOpacity: Float
    {
        set (newValue) {
            self.layer.shadowOpacity = newValue
        }
        
        get {
            return self.layer.shadowOpacity
        }
    }
    
    func translateShowAnimation(_ type: AnimationType, _ duration:TimeInterval = 0.3, _ delay:TimeInterval = 0.1) {
        self.alpha = 0
        self.isHidden = true
        self.transform = CGAffineTransform(translationX: 0, y: type == .top ? 0 - self.frame.size.height : UIScreen.main.bounds.size.height)
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            
            let translation = CGAffineTransform(translationX: 0, y: 0)
            let scale = CGAffineTransform(scaleX: 1, y: 1)
            
            self.transform = translation.concatenating(scale)
            self.alpha = 1
            self.isHidden = false
            
        }, completion: nil)
    }
    
    func translateHideAnimation(_ type: AnimationType, _ duration:TimeInterval = 0.3, _ delay:TimeInterval = 0.1) {
        self.alpha = 1
        self.isHidden = false
        self.transform = CGAffineTransform(translationX: 0, y: 0)
        
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            
            self.transform = CGAffineTransform(translationX: 0, y: type == .top ? 0 - self.frame.size.height : UIScreen.main.bounds.size.height - self.frame.size.height)
            self.alpha = 0
            self.isHidden = true
            
        }, completion: nil)
    }
    
    func performShakeAnimation() {
        self.layer.removeAnimation(forKey: "anim")
        let animation = CAKeyframeAnimation(keyPath: "transform")
        animation.duration = 0.5
        animation.values = [
            NSValue(caTransform3D: self.layer.transform),
            NSValue(caTransform3D: CATransform3DScale(self.layer.transform, 1.05, 1.05, 1.0)),
            NSValue(caTransform3D: CATransform3DScale(self.layer.transform, 0.95, 0.95, 1.0)),
            NSValue(caTransform3D: self.layer.transform)
        ]
        animation.isRemovedOnCompletion = true
        self.layer.add(animation, forKey: "anim")
    }
    
    func performTransitionCrossDissolveAnimation(completion:@escaping() -> ())
    {
        UIView.transition(with: self,
                          duration: 0.5,
                          options: .transitionCrossDissolve,
                          animations: {  completion () },
                          completion: nil)
    }
    
    
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
    
}

extension UIView {
    
    // In order to create computed properties for extensions, we need a key to
    // store and access the stored property
    fileprivate struct AssociatedObjectKeys {
        static var tapGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewer"
        static var panGestureRecognizer = "MediaViewerAssociatedObjectKey_mediaViewerPan"
    }
    
    fileprivate typealias Action = (() -> Void)?
    fileprivate typealias PanAction = ((UIPanGestureRecognizer) -> Void)?
    
    // Set our computed property type to a closure
    fileprivate var tapGestureRecognizerAction: Action? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.tapGestureRecognizer) as? Action
            return tapGestureRecognizerActionInstance
        }
    }
    
    // Set our computed property type to a closure
    fileprivate var panGestureRecognizerAction: PanAction? {
        set {
            if let newValue = newValue {
                // Computed properties get stored as associated objects
                objc_setAssociatedObject(self, &AssociatedObjectKeys.panGestureRecognizer, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
            }
        }
        get {
            let tapGestureRecognizerActionInstance = objc_getAssociatedObject(self, &AssociatedObjectKeys.panGestureRecognizer) as? PanAction
            return tapGestureRecognizerActionInstance
        }
    }
    
    // This is the meat of the sauce, here we create the tap gesture recognizer and
    // store the closure the user passed to us in the associated object we declared above
    public func addTapGestureRecognizer(action: (() -> Void)?) {
        self.isUserInteractionEnabled = true
        self.tapGestureRecognizerAction = action
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    public func addPanGestureRecognizer(action: ((UIPanGestureRecognizer) -> Void)?) {
        self.isUserInteractionEnabled = true
        self.panGestureRecognizerAction = action
        let tapGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // Every time the user taps on the UIImageView, this function gets called,
    // which triggers the closure we stored
    @objc fileprivate func handleTapGesture(sender: UITapGestureRecognizer) {
        if let action = self.tapGestureRecognizerAction {
            action?()
        } else {
            print("no action")
        }
    }
    
    @objc fileprivate func handlePanGesture(sender: UIPanGestureRecognizer) {
        if let action = self.panGestureRecognizerAction {
            action?(sender)
        } else {
            print("no action")
        }
    }
    
}

extension UINavigationController {
    func customAppearance() {
        navigationBar.barTintColor = .white

        // To change colour of tappable items.
        navigationBar.tintColor = UIColor(named: "clrViolet")

        // To apply textAttributes to title i.e. colour, font etc.
        navigationBar.titleTextAttributes = [.foregroundColor : UIColor(named: "clrBlack") ?? .black,
                                                            .font : UIFont.init(name: "Poppins-Medium", size: 18.3)!]
        // To control navigation bar's translucency.
        navigationBar.isTranslucent = false
    }
}

extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
    
    func toHexString() -> String {
        //        let components = self.cgColor.components
        //        let r: CGFloat? = components?[0]
        //        let g: CGFloat? = components?[1]
        //        let b: CGFloat? = components?[2]
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        let hexString = String(format: "%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        return hexString
    }
    
    class func color(withData data:Data) -> UIColor {
        return NSKeyedUnarchiver.unarchiveObject(with: data) as! UIColor
    }
    
    func encode() -> Data {
        return NSKeyedArchiver.archivedData(withRootObject: self)
    }
}

extension String
{
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }

    func loadFontName() throws -> String? {
        
        guard let bundleUrl = Bundle.main.url(forResource: "Fonts", withExtension: "bundle") else {
            return nil
        }
        
        let fontFileURL = bundleUrl.appendingPathComponent(self)
        
        let fontData = try Data(contentsOf: fontFileURL)
        
        guard let provider = CGDataProvider(data: fontData as CFData) else {
            return nil
        }
        
        guard let cgFont = CGFont(provider) else {
            return nil
        }
        
        guard let fontPostScriptName = cgFont.postScriptName else {
            return nil
        }
        
        guard let fontName = cgFont.fullName else {
            return nil
        }
        
        return (fontPostScriptName as String) + "=" + (fontName as String)
    }
    
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
}

protocol ResetValues:AnyObject {
    func resetTextValues(_ str:String?, _ newAttributes:[AnyHashable : Any]?)
}

extension ResetValues {
    func resetTextValues(_ str:String?, _ newAttributes:[AnyHashable : Any]?) {
        var attributesDict = [AnyHashable : Any]()
        let attributedText:NSAttributedString = ((self is UITextView) ? (self as? UITextView)!.attributedText : (self as? UILabel)!.attributedText)!
        attributedText.enumerateAttributes(in: NSRange(location: 0, length: (attributedText.string.count)), options: .longestEffectiveRangeNotRequired, using: { attributes, range, stop in
            attributesDict = attributes
        })
        if newAttributes != nil {
            attributesDict.merge(dict: newAttributes!)
        }
        let mutableString = NSMutableAttributedString(string: str ?? attributedText.string, attributes: attributesDict as! [NSAttributedString.Key : Any])
        if let txtView = self as? UITextView {
            txtView.attributedText = mutableString
        }else {
            if let lbl = self as? UILabel {
                lbl.attributedText = mutableString
            }
        }
    }
}

extension UITextView: ResetValues {}

extension UILabel: ResetValues {}

extension UITextView {
    func setUpLink(_ strText:String, _ linkUrl:String, _ alignment:NSTextAlignment)
    {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        let linkAttributes = [NSAttributedString.Key.link : NSURL(string: linkUrl)!, NSAttributedString.Key.paragraphStyle : paragraphStyle] as [NSAttributedString.Key : Any]
        let attributedString = NSMutableAttributedString.init(string: strText, attributes: linkAttributes)
        self.attributedText = attributedString
        self.linkTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.paragraphStyle : paragraphStyle]
        self.textColor = .darkGray
        self.font = FontHelper.semiboldFontWithSize(size: 13)
    }
}

extension UIWindow {
    
    var topViewController: UIViewController? {
        if var topController = Constants.appDelegate.window?.rootViewController!.presentedViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            return topController
        }
        return nil
    }
}

extension UINavigationController
{
    func setUpNavigation(_ isHidden:Bool, _ barColor:UIColor = #colorLiteral(red: 0.4972119331, green: 0.2973102629, blue: 1, alpha: 1), _ fontColor:UIColor = .white, _ font:UIFont = FontHelper.semiboldFontWithSize(size: 17)){
        self.setNavigationBarHidden(isHidden, animated: true)
        self.navigationBar.barTintColor = barColor
        self.navigationBar.isTranslucent = false
        self.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: fontColor,
             NSAttributedString.Key.font: font]
    }
}

extension UIViewController
{
    //TODO: Added child VC
    func addChildController(controller : UIViewController) {
        
        self.addChild(controller)
        self.view.addSubview(controller.view)
        controller.view.frame = self.view.bounds
        controller.view.alpha = 0
        controller.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.5, delay: 0.1, options: .transitionFlipFromBottom, animations: {
            controller.view.alpha = 1
        }) { (finished) in
            controller.didMove(toParent: self)
        }
    }
    
    //TODO: Remove added child VC
    func removeChildController(controller : UIViewController) {
        
        controller.willMove(toParent: nil)
        controller.view.removeFromSuperview()
        controller.removeFromParent()
    }
    
    func showAlert(_ title:String, _ msg:String) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        
        if let topController = Constants.appDelegate.window?.topViewController {
            topController.present(alert, animated: true, completion: nil)
        }else {
            Constants.appDelegate.window?.rootViewController?.present(alert, animated: true, completion: nil)
        }
    }
}

extension CGAffineTransform {
    var angle: CGFloat { return atan2(-self.c, self.a) }
    
    var angleInDegrees: CGFloat { return self.angle * 180 / .pi }
    
    var scaleX: CGFloat {
        let angle = self.angle
        return self.a * cos(angle) - self.c * sin(angle)
    }
    
    var scaleY: CGFloat {
        let angle = self.angle
        return self.d * cos(angle) + self.b * sin(angle)
    }
}

extension UIImage
{
    func createFilteredImage(filterName: String) -> UIImage {
        // 1 - create source image
        let sourceImage = CIImage(image: self)
        
        // 2 - create filter using name
        let filter = CIFilter(name: filterName)
        filter?.setDefaults()
        
        // 3 - set source image
        filter?.setValue(sourceImage, forKey: kCIInputImageKey)
        
        let context = CIContext(options: nil)
        
        // 4 - output filtered image as cgImage with dimension.
        if let outputImage = filter?.outputImage {
            let outputCGImage = context.createCGImage(outputImage, from: outputImage.extent)
            
            // 5 - convert filtered CGImage to UIImage
            let filteredImage = UIImage.init(cgImage: outputCGImage!, scale: self.scale, orientation: self.imageOrientation)
            
            return filteredImage
        }
        return self
    }
}

extension UIView {
    
    
    /* Usage Example
     * bgView.addBottomRoundedEdge(desiredCurve: 1.5)
     */
    func addBottomRoundedEdge(desiredCurve: CGFloat?) {
        let offset: CGFloat = self.frame.width / desiredCurve!
        let bounds: CGRect = self.bounds
        
        let rectBounds: CGRect = CGRect(x: bounds.origin.x, y: bounds.origin.y, width: bounds.size.width, height: bounds.size.height / 2)
        let rectPath: UIBezierPath = UIBezierPath(rect: rectBounds)
        let ovalBounds: CGRect = CGRect(x: bounds.origin.x - offset / 2, y: bounds.origin.y, width: bounds.size.width + offset, height: bounds.size.height)
        let ovalPath: UIBezierPath = UIBezierPath(ovalIn: ovalBounds)
        rectPath.append(ovalPath)
        
        // Create the shape layer and set its path
        let maskLayer: CAShapeLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = rectPath.cgPath
        
        // Set the newly created shape layer as the mask for the view's layer
        self.layer.mask = maskLayer
    }
    
    func applyGradient(colors : [CGColor], shadowColor : CGColor) {
        self.backgroundColor = .clear
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colors
        gradient.startPoint = CGPoint(x: 0, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        
        addShadow(offset: CGSize(width: 5.0, height: 5.0), color: shadowColor, radius: 10, opacity: 0.5)
        
        let containerView = UIView()
        // set the cornerRadius of the containerView's layer
        containerView.layer.cornerRadius = 10.0
        containerView.layer.masksToBounds = true
        
        insertSubview(containerView, at: 0)
        
        // add constraints
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // pin the containerView to the edges to the view
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        //apply gradient
        containerView.layer.insertSublayer(gradient, at: 0)
    }
    
    func addShadow(offset: CGSize, color: CGColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
}

extension UIView {
    class func fromNib(named: String? = nil, owner : UIViewController? = nil) -> Self {
        let name = named ?? "\(Self.self)"
        guard
            let nib = Bundle.main.loadNibNamed(name, owner: owner, options: nil)
            else { fatalError("missing expected nib named: \(name)") }
        guard
            /// we're using `first` here because compact map chokes compiler on
            /// optimized release, so you can't use two views in one nib if you wanted to
            /// and are now looking at this
            let view = nib.first as? Self
            else { fatalError("view of type \(Self.self) not found in \(nib)") }
        return view
    }
    
    func asImage() -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(self.frame.size)
            self.layer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
    }

}

enum RemoveBackroundResult {
    case background
    case finalImage
}

extension UIImage {

    func removeBackground(returnResult: RemoveBackroundResult) -> UIImage? {
        guard let model = getDeepLabV3Model() else { return nil }
        let width: CGFloat = 513
        let height: CGFloat = 513
        let resizedImage = resized(to: CGSize(width: height, height: height), scale: 1)
        guard let pixelBuffer = resizedImage.pixelBuffer(width: Int(width), height: Int(height)),
        let outputPredictionImage = try? model.prediction(image: pixelBuffer),
        let outputImage = outputPredictionImage.semanticPredictions.image(min: 0, max: 1, axes: (0, 0, 1)),
        let outputCIImage = CIImage(image: outputImage),
        let maskImage = outputCIImage.removeWhitePixels(),
        let maskBlurImage = maskImage.applyBlurEffect() else { return nil }

        switch returnResult {
        case .finalImage:
            guard let resizedCIImage = CIImage(image: resizedImage),
                  let compositedImage = resizedCIImage.composite(with: maskBlurImage) else { return nil }
            let finalImage = UIImage(ciImage: compositedImage)
                .resized(to: CGSize(width: size.width, height: size.height))
            return finalImage
        case .background:
            let finalImage = UIImage(
                ciImage: maskBlurImage,
                scale: scale,
                orientation: self.imageOrientation
            ).resized(to: CGSize(width: size.width, height: size.height))
            return finalImage
        }
    }

    private func getDeepLabV3Model() -> DeepLabV3? {
        do {
            let config = MLModelConfiguration()
            return try DeepLabV3(configuration: config)
        } catch {
            print("Error loading model: \(error)")
            return nil
        }
    }

}

extension CIImage {

    func removeWhitePixels() -> CIImage? {
        let chromaCIFilter = chromaKeyFilter()
        chromaCIFilter?.setValue(self, forKey: kCIInputImageKey)
        return chromaCIFilter?.outputImage
    }

    func composite(with mask: CIImage) -> CIImage? {
        return CIFilter(
            name: "CISourceOutCompositing",
            parameters: [
                kCIInputImageKey: self,
                kCIInputBackgroundImageKey: mask
            ]
        )?.outputImage
    }

    func applyBlurEffect() -> CIImage? {
        let context = CIContext(options: nil)
        let clampFilter = CIFilter(name: "CIAffineClamp")!
        clampFilter.setDefaults()
        clampFilter.setValue(self, forKey: kCIInputImageKey)

        guard let currentFilter = CIFilter(name: "CIGaussianBlur") else { return nil }
        currentFilter.setValue(clampFilter.outputImage, forKey: kCIInputImageKey)
        currentFilter.setValue(2, forKey: "inputRadius")
        guard let output = currentFilter.outputImage,
              let cgimg = context.createCGImage(output, from: extent) else { return nil }

        return CIImage(cgImage: cgimg)
    }

    // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
    private func chromaKeyFilter() -> CIFilter? {
        let size = 64
        var cubeRGB = [Float]()

        for z in 0 ..< size {
            let blue = CGFloat(z) / CGFloat(size - 1)
            for y in 0 ..< size {
                let green = CGFloat(y) / CGFloat(size - 1)
                for x in 0 ..< size {
                    let red = CGFloat(x) / CGFloat(size - 1)
                    let brightness = getBrightness(red: red, green: green, blue: blue)
                    let alpha: CGFloat = brightness == 1 ? 0 : 1
                    cubeRGB.append(Float(red * alpha))
                    cubeRGB.append(Float(green * alpha))
                    cubeRGB.append(Float(blue * alpha))
                    cubeRGB.append(Float(alpha))
                }
            }
        }
        
//        let data = UnsafeBufferPointer(start: &cubeRGB, count: cubeRGB.count)
        //let data = Data(buffer: UnsafeBufferPointer<Float>(start: &cubeRGB, count: cubeRGB.count))
        // approach #1
        var data = Data()
        cubeRGB.withUnsafeBufferPointer { ptr in
            data = Data(buffer: ptr)
        }

        
        //let data = NSData(bytes: &cubeRGB, length: cubeRGB.count)

        
        let colorCubeFilter = CIFilter(
            name: "CIColorCube",
            parameters: [
                "inputCubeDimension": size,
                "inputCubeData": data
            ]
        )
        return colorCubeFilter
    }

    // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
    private func getBrightness(red: CGFloat, green: CGFloat, blue: CGFloat) -> CGFloat {
        let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
        var brightness: CGFloat = 0
        color.getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
        return brightness
    }

}
