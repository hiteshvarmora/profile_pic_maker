//
//  Functions.swift
//  Logo_Maker
//
//  Created by SOTSYS104 on 04/02/19.
//  Copyright © 2019 Dave. All rights reserved.
//

import UIKit
import StoreKit
import Photos

var hud:LottieHUD!
func readLocalPlist(_ plistName:String) -> Any?
{
    var result: Any?
    if let path = Bundle.main.path(forResource: plistName, ofType: "plist") {
        result = NSDictionary(contentsOfFile: path)
        if result == nil {
            result = NSArray(contentsOfFile: path)
        }
        return result
    }
    return nil
}

func readPlistFromDocumentDirectory(_ storedPath:String) -> Any?
{
    var result: Any?
    let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let fileURL = documentsURL.appendingPathComponent(storedPath)
    
    if FileManager.default.fileExists(atPath: fileURL.path) {
        result = NSDictionary(contentsOfFile: fileURL.path)
        if result == nil {
            result = NSArray(contentsOfFile: fileURL.path)
        }
        return result
    }
    return nil
}

func delay(_ delay: Double, closure: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
        closure()
    }
}

func animate(_ duration: Double, closure: @escaping () -> ()) {
    UIView.animate(withDuration: duration) {
        closure()
    }
}

func hideShowLottieHUD(_ isShow:Bool = true) {
    if isShow {
        hud = LottieHUD("loader", size: CGSize(width: 200, height: 200))
        hud.showHUD()
    }else {
        if hud != nil {
            hud.stopHUD()
        }
    }
}

func fetchImage(asset: PHAsset, contentMode: PHImageContentMode, targetSize: CGSize, completionHandler : @escaping (UIImage?) -> ()) {
    let options = PHImageRequestOptions()
    options.version = .current
    options.isNetworkAccessAllowed = true
    options.isSynchronous = true
    options.resizeMode = .none
    options.deliveryMode = .highQualityFormat
    
    //let screenSize: CGSize = UIScreen.main.bounds.size
    //let targetSize1 = CGSize(width: screenSize.width, height: screenSize.height) //CGSizeMake(screenSize.width, screenSize.height)
    PHImageManager.default().requestImage(for: asset, targetSize: targetSize, contentMode: contentMode, options: options) { image, info in
        
        if let info = info, info["PHImageFileUTIKey"] == nil {
            if let isDegraded = info[PHImageResultIsDegradedKey] as? Bool, isDegraded {
                //Here is Low quality image , in this case return
                print("PHImageResultIsDegradedKey =======> \(isDegraded)")
                return
            }
            //Here you got high resilutions image
        }
        
        guard let image = image else {
            completionHandler(nil)
            return
        }
        
        completionHandler(image)
    }
    
}

func showAlert(_ msg: String?) {
    let ac = UIAlertController.init(title: APPNAME, message: msg, preferredStyle: .alert)
    let ok = UIAlertAction.init(title: "OK", style: .cancel, handler: nil)
    ac.addAction(ok)
    Constants.appDelegate.window?.rootViewController?.present(ac, animated: true, completion: nil)
}


func hexStringToUIColor(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }

    if ((cString.count) != 6) {
        return UIColor.gray
    }

    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
    let size = image.size
    
    let widthRatio  = targetSize.width  / size.width
    let heightRatio = targetSize.height / size.height
    
    // Figure out what our orientation is, and use that to form the rectangle
    var newSize: CGSize
    if(widthRatio > heightRatio) {
        newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
    } else {
        newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
    }
    
    // This is the rect that we've calculated out and this is what is actually used below
    let rect = CGRect(origin: .zero, size: newSize)
    
    // Actually do the resizing to the rect using the ImageContext stuff
    UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
    image.draw(in: rect)
    let newImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    
    return newImage
}
