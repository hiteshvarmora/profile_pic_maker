//
//  InAppPriceModel.swift
//  Logo_Maker
//
//  Created by SOTSYS038 on 29/01/19.
//  Copyright © 2019 Highbroworks LLP. All rights reserved.
//

import UIKit
import StoreKit

class InAppPriceModel {
    var productID = ""
    var price = "0"
    var formattedPrice = ""
    var introductoryPrice = ""
    var currencycode = ""
    
    init(productID : String, price : String, introductoryPrice : String, currencycode : String, formattedPrice : String ) {
        self.productID = productID
        self.price = price
        self.formattedPrice = formattedPrice
        self.introductoryPrice = introductoryPrice
        self.currencycode = currencycode
    }
    
    convenience init(product : SKProduct) {
        
        var formattedPrice: String? = nil
        var productPrice: String? = nil
        let price = product.price
        
        if price == NSDecimalNumber(decimal: 0.00) {
            formattedPrice = "0.0"
            productPrice = "0.0"
        } else {
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = product.priceLocale
            formattedPrice = numberFormatter.string(from: price)
            
            numberFormatter.numberStyle = .none
            productPrice = numberFormatter.string(from: price)
        }
        let currency = product.priceLocale.currencyCode
        
        self.init(productID: product.productIdentifier, price: productPrice!, introductoryPrice: InAppShared.shared.getIntroductoryPrices(product),currencycode: currency ?? "USD", formattedPrice:formattedPrice!)
    }
}

extension Array where Element:InAppPriceModel
{
    func getPrices()->(weeklyPrice : String, monthlyPrice : String)
    {
        var weeklyPrice : String = ""
        var monthlyPrice : String = ""

        for objInAppPrices in self
        {
            switch objInAppPrices.productID
            {
            case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                weeklyPrice = objInAppPrices.price
            case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                monthlyPrice = objInAppPrices.price
            default: break
                
            }
        }
        return(weeklyPrice,monthlyPrice)
    }
    
    func getFormattedPrices()->(weeklyPrice : String, monthlyPrice : String)
    {
        var weeklyPrice : String = ""
        var monthlyPrice : String = ""
        
        for objInAppPrices in self
        {
            switch objInAppPrices.productID
            {
            case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                weeklyPrice = objInAppPrices.formattedPrice
            case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                monthlyPrice = objInAppPrices.formattedPrice
            default: break
                
            }
        }
        return(weeklyPrice,monthlyPrice)
    }
    
    func getIntroductoryPrices()->(weekly : String, monthly : String)
    {
        var weekly : String = ""
        var monthly : String = ""
        for objInAppPrices in self
        {
            switch objInAppPrices.productID
            {
            case WEEKLY_SUBSCRIPTION_PRODUCT_ID:
                weekly = objInAppPrices.introductoryPrice
            case YEARLY_SUBSCRIPTION_PRODUCT_ID:
                monthly = objInAppPrices.introductoryPrice
            default: break
                
            }
        }
        return(weekly,monthly)
    }
    
    func getCurrencyCode()->String
    {
        return self.count > 0 ? self[0].currencycode : "USD"
    }
}
