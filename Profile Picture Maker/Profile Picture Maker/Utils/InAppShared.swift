//
//  InAppShared.swift
//  Invite Maker
//
//  Created by SOTSYS220 on 07/07/18.
//  Copyright © 2018 Highbroworks LLP. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import Purchases

@objc public class InAppShared : NSObject,PurchaseProtocol {
    
    @objc public static var shared = InAppShared()
    
    @objc public var inAppIds = [WEEKLY_SUBSCRIPTION_PRODUCT_ID,YEARLY_SUBSCRIPTION_PRODUCT_ID]
    fileprivate var sharedSecret = IN_APP_SHARED_SECRET
    
    @objc public var currentID = WEEKLY_SUBSCRIPTION_PRODUCT_ID
    @objc public var currentPrice = "2.99"
    
    @objc public func setupIAP() {
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    print("\(purchase.transaction.transactionState.debugDescription): \(purchase.productId)")
                case .failed, .purchasing, .deferred:
                    break // do nothing
                @unknown default:
                    break
                }
            }
        }
        
        SwiftyStoreKit.updatedDownloadsHandler = { downloads in
            
            // contentURL is not nil if downloadState == .finished
            let contentURLs = downloads.compactMap { $0.contentURL }
            if contentURLs.count == downloads.count {
                print("Saving: \(contentURLs)")
                SwiftyStoreKit.finishTransaction(downloads[0].transaction)
            }
        }
        
        SwiftyStoreKit.shouldAddStorePaymentHandler = { payment, product in
            print(payment,product)
            
            self.openPurchaseController(isFromStore: true)
            
            return false
        }
    }
    
    @objc public func getPriceInfo(isRevenueCat:Bool,completion : @escaping ([SKProduct]) -> Void) {
            Purchases.shared.offerings { (offerings, error) in
                if let e = error {
                    print(e.localizedDescription)
                }
                
                guard let offering = offerings?.current else {
                    print("No current offering configured")
                    return
                }
                Constants.appDelegate.offering = offerings?.current

                var arrSKProduct = [SKProduct]()
                for package in offering.availablePackages {
                    print("Product: \(package.product.localizedDescription), price: \(package.localizedPriceString)")
                    arrSKProduct.append(package.product)
                }
                completion(arrSKProduct)
            }
    }
    
    public func purchase(isRevenueCat:Bool,completion: @escaping (SKProduct,SKPaymentTransaction,Bool) -> Void,errorBlock:  @escaping  (String) -> Void) {
            let productIdentifier = currentID
            var packageIndex = -1
            if let packageArr = Constants.appDelegate.offering?.availablePackages{
                for package in packageArr {
                    if package.product.productIdentifier == productIdentifier {
                        packageIndex = packageArr.firstIndex(of: package)!
                    }
                }
            }
            guard let package =  Constants.appDelegate.offering?.availablePackages[packageIndex] else {
                print("No available package")
                return
            }
            Purchases.shared.purchasePackage(package) { (transaction, info, error, cancelled) in
                if cancelled {
                    print("User cancelled purchase")
                    errorBlock("User cancelled purchase")
                }
                if let err = error as NSError? {
                    print("RCError: \(err)")
                    errorBlock(err.localizedDescription)
                } else if info?.entitlements.all["Unlimited Access"]?.isActive == true {
                    print("Unlocked Pro 🎉")
                    completion(package.product,transaction!,true)
                }
            }
    }
    
    public func restoreTransaction(isRevenueCat:Bool,completion: @escaping (Bool,String) -> Void) {
        if isRevenueCat{
            Purchases.shared.restoreTransactions { (purchaserInfo, error) in
                if let e = error {
                    print("Restore Failed: \(e.localizedDescription)")
                    completion(false,"Restore failed, Unknown error. Please contact support")
                } else {
                    if let purchaserInfo = purchaserInfo {
                        if purchaserInfo.entitlements.active.isEmpty {
                                print("Restore Unsuccessful No prior purchases found for your account.")
                                //self.showAlert(title: "Restore Unsuccessful", message: "No prior purchases found for your account.")
                             completion(false,"Restore Unsuccessful No prior purchases found for your account.")
                        }
                        else{
                            if (purchaserInfo.entitlements["Unlimited Access"]?.productIdentifier) != nil{
                                print("Restore Success")
                                completion(true,"Purchases Restored, All purchases have been restored")
                            }
                        }
                    }
                }
            }
        }
        else{
            SwiftyStoreKit.restorePurchases(atomically: true) { results in
                
                for purchase in results.restoredPurchases {
                    let downloads = purchase.transaction.downloads
                    if !downloads.isEmpty {
                        SwiftyStoreKit.start(downloads)
                    } else if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                }
                
                if results.restoreFailedPurchases.count > 0 {
                    print("Restore Failed: \(results.restoreFailedPurchases)")
                    completion(false,"Restore failed, Unknown error. Please contact support")
                } else if results.restoredPurchases.count > 0 {
                    print("Restore Success: \(results.restoredPurchases)")
                    //self.currentID = results.restoredPurchases[0].productId
                    completion(true,"Purchases Restored, All purchases have been restored")
                } else {
                    print("Nothing to Restore")
                    completion(false,"Nothing to restore, No previous purchases were found")
                }
            }
        }
        
    }
    
    public func verifyReceipt(isRevenueCat:Bool,completion: @escaping (Bool,Date) -> Void,errorBlock:  @escaping  (String,Int) -> Void) {
            Purchases.shared.purchaserInfo { (purchaserInfo, error) in
                if let productPurchased = purchaserInfo?.entitlements.all["UNLIMITED ACCESS"]?.productIdentifier{
                    print("Purchased Product: \(productPurchased)")
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                if let purchaseDate = purchaserInfo?.purchaseDate(forEntitlement: "UNLIMITED ACCESS") {
                    print("Purchase Date: \(dateFormatter.string(from: purchaseDate))")
                }
                if let expirationDate = purchaserInfo?.expirationDate(forEntitlement: "UNLIMITED ACCESS") {
                    print("Expiration Date: \(dateFormatter.string(from: expirationDate))")
                }
                let expirationDate = purchaserInfo?.expirationDate(forEntitlement: "UNLIMITED ACCESS")
                if purchaserInfo?.entitlements.all["UNLIMITED ACCESS"]?.isActive == true {
                    // Grant user "pro" access
                    completion(true, expirationDate ?? Date())
                }
                else{
                    completion(false, expirationDate ?? Date())
                }
            }
    }
        
    func getIntroductoryPrices(_ product: SKProduct)->String
    {
        var strUnit = ""
        if #available(iOS 11.2, *) {
            if let introductoryPrices = product.introductoryPrice
            {
                let unit = introductoryPrices.subscriptionPeriod.unit.rawValue
                let numberOfUnits = introductoryPrices.subscriptionPeriod.numberOfUnits
                switch unit
                {
                case 0:
                    strUnit = "\(numberOfUnits) " + "day"
                case 1:
                    strUnit = "\(numberOfUnits) " + "week"
                case 2:
                    strUnit = "\(numberOfUnits) " + "month"
                default:
                    strUnit = "\(numberOfUnits) " + "year"
                }
                print(strUnit)
            }
        }
        return strUnit
    }
}
