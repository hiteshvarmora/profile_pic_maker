//
//  PurchaseProtocol.swift
//  Poster
//
//  Created by SOTSYS220 on 28/09/18.
//  Copyright © 2018 Highbroworks LLP. All rights reserved.
//

import UIKit

protocol PurchaseProtocol {
    func openPurchaseController(isFromStore : Bool)
}

extension PurchaseProtocol {
    
    func openPurchaseController(isFromStore : Bool) {
        let sceneDelegate = UIApplication.shared.connectedScenes
            .first!.delegate as! SceneDelegate

        if (sceneDelegate.window?.rootViewController?.presentedViewController as? SalesPageView) != nil {
            return
        }
        
        let obj = StoryBoardName.sales.instantiateViewController(withIdentifier: "SalesPageView") as? SalesPageView
        obj?.isFromAppStore = isFromStore
        if let anObj = obj {
            
            let aNavController = UINavigationController(rootViewController: anObj)
            //aNavController.modalPresentationStyle = .fullScreen
            aNavController.navigationBar.isHidden = true
            
            if let pc =  sceneDelegate.window?.rootViewController?.presentedViewController {
                if let navigationController = pc as? UINavigationController {
                    navigationController.visibleViewController?.present(aNavController, animated: true, completion: nil)
                    return
                }
                pc.present(aNavController, animated: true, completion: nil)
            } else {
                sceneDelegate.window?.rootViewController?.present(aNavController, animated: true, completion: nil)
            }
        }
    }
    
    func openWeeklyDiscountPurchaseController() {
        /*
        if (AppDelegate.shared.window!.rootViewController?.presentedViewController as? DiscountedSalesVC) != nil {
            return
        }
        if (AppDelegate.shared.window!.rootViewController?.presentedViewController as? SalesVC) != nil || (AppDelegate.shared.window!.rootViewController?.presentedViewController as? NewSalesVC) != nil  {
            AppDelegate.shared.window!.rootViewController?.presentedViewController?.dismiss(animated: true, completion: {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let obj = storyboard.instantiateViewController(withIdentifier: "DiscountedSalesVC") as? DiscountedSalesVC
                
                if let anObj = obj {
                    if let pc =  AppDelegate.shared.window!.rootViewController?.presentedViewController {
                        pc.present(anObj, animated: true, completion: nil)
                    }else{
                        AppDelegate.shared.window!.rootViewController?.present(anObj, animated: true, completion: nil)
                    }
                }
                return
            })
        }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let obj = storyboard.instantiateViewController(withIdentifier: "DiscountedSalesVC") as? DiscountedSalesVC
        
        if let anObj = obj {
            if let pc =  AppDelegate.shared.window!.rootViewController?.presentedViewController {
                pc.present(anObj, animated: true, completion: nil)
            }else{
                AppDelegate.shared.window!.rootViewController?.present(anObj, animated: true, completion: nil)
            }
        }
        */
    }
}
