//
//  Structs.swift
//  Thumbnail Maker
//
//  Created by SOTSYS104 on 27/04/19.
//  Copyright © 2019 SOTSYS104. All rights reserved.
//

import UIKit

struct ScreenSize {
    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
    static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH   >= 1024.0
    static let IS_IPAD_PRO          = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH   == 1366.0
    static let IS_IPHONE_XSMAX      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 2688
    static let IS_IPHONE_XR         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 1792
    static let IS_IPHONE_X_OR_G     = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH >= 812
}

struct Constants {
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let bundleName = Bundle.main.infoDictionary?["CFBundleName"] as! String
    
    static var timestamp: String {
        return "\(NSDate().timeIntervalSince1970)"
    }
}

struct StoryBoardName {
    static let home = UIStoryboard(name: "HomeView", bundle: nil)
    static let chooseProfile = UIStoryboard(name: "ChooseProfileView", bundle: nil)
    static let exportProfile = UIStoryboard(name: "ExportProfileView", bundle: nil)
    static let editProfile = UIStoryboard(name: "EditProfileView", bundle: nil)
    static let introScreen = UIStoryboard(name: "IntroScreenView", bundle: nil)
    static let previewView = UIStoryboard(name: "PreviewView", bundle: nil)
    static let main = UIStoryboard(name: "Main", bundle: nil)
    static let sales = UIStoryboard(name: "SalesPageView", bundle: nil)
}

struct CanvasActualSize {
    static let artSize = CGSize(width: 2560, height: 1440)
    static let thumbnailSize = CGSize(width: 1280, height: 720)
}

struct FontHelper {
    
    static func regularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Regular", size: size + (DeviceType.IS_IPAD ? 3 : DeviceType.IS_IPHONE_5 ? -2 : 0))!
    }
    
    static func semiboldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Semibold", size: size + (DeviceType.IS_IPAD ? 3 : DeviceType.IS_IPHONE_5 ? -2 : 0))!
    }
    
    static func boldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: size + (DeviceType.IS_IPAD ? 3 : DeviceType.IS_IPHONE_5 ? -2 : 0))!
    }
    
    static func avenirNextsemiboldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "AvenirNextRoundedStd-Med", size: size)!
    }
}

struct SalesData {
    static let arrData = ["Unlock all templates".localized, "Unlock all 1000+ backgrounds".localized, "Unlock all 500+ fonts".localized, "Unlock all 5000+ graphics, filters & overlay".localized, "Save without watermark".localized]
    static let TermsOfService = "https://eagleedge.co.in/term-condition.html"
    static let PrivacyPolicy = "https://eagleedge.co.in/privacy-policy.html"
}

struct WatermarkText {
    static let str1 = "Made with".localized
    static let str2 = APPNAME
    static let str3 = "Tap to remove watermark".localized
}

struct SegueIdentifier {
    static let text    = "Text"
    static let sticker = "Sticker"
    static let layers  = "Layers"
}

struct Firebase {

    static let imageToken  = "?alt=media&token=7309e6f6-c5b2-49e0-aeff-21083f64d506"
    static let liveURL  = "https://firebasestorage.googleapis.com/v0/b/thumbnail-maker-222c2.appspot.com/"
    static let storageLiveRef  = "gs://thumbnail-maker-222c2.appspot.com"
    static let databaseLiveRef = "https://thumbnail-maker-222c2.firebaseio.com/"
}

struct Messages {
    static let restoreSuccess     = "Restore purchase successfully.".localized
    static let restoreFailed      = "Your purchase may expired. Please resubscribe to use pro feature.".localized
}

struct ConstantsPP {
    
    static let kIntroCell = "IntroCell"
    
    //Mark:- Intro screen data
    static let kIntroImage_1 = "img_Intro1"
    static let kIntroTitle_1 = "CREATE PERFECT"
    static let kIntroDescription_1 = "PROFILE PICTURE"
    static let kIntroBackgroundColor_1 = UIColor.init(red: 255.0/255.0, green: 228.0/255.0, blue: 233.0/255.0, alpha: 1.0)
    
    static let kIntroImage_2 = "img_Intro2"
    static let kIntroTitle_2 = "AUTOMATIC"
    static let kIntroDescription_2 = "BACKGROUND REMOVER"
    static let kIntroBackgroundColor_2 = UIColor.init(red: 234.0/255.0, green: 246.0/255.0, blue: 254.0/255.0, alpha: 1.0)
    
    static let kIntroImage_3 = "img_Intro3"
    static let kIntroTitle_3 = "CHOOSE ELEGANT"
    static let kIntroDescription_3 = "BACKGROUND"
    static let kIntroBackgroundColor_3 = UIColor.init(red: 253.0/255.0, green: 229.0/255.0, blue: 196.0/255.0, alpha: 1.0)
}
