//
//  UserDefaults.swift
//  Poster
//
//  Created by SOTSYS220 on 28/09/18.
//  Copyright © 2018 Dave. All rights reserved.
//

import UIKit

extension UserDefaults {
    
    var isOnboardingShown : Bool {
        get{
            return bool(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
            self.synchronize()
        }
    }
    
    var isSubscribed : Bool {
        get{
            return bool(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
        }
    }
    

    var isYearly : Bool {
        get{
            return bool(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
        }
    }
        
    var isWeekly : Bool {
        get{
            return bool(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
        }
    }
        
    var subscriptionDate : Any? {
        get{
            return object(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
        }
    }

    var isAppleReview : Bool {
        get{
            return bool(forKey: #function)
        }
        set{
            set(newValue, forKey: #function)
            self.synchronize()
        }
    }
}
